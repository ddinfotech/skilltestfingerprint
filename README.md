## Working Flow Of APP

## -- Biometric Advanced --##

# Description
- Used for verified Biometric Candidate detail which already captured.
- This app works in Two Mode first online and second offline.

# Steps for Offline
1. Need to copy all json file in External Storage. Files are -  LoginDetail.json, CandidateFingerDetail.json and CandidateDetail.json.
2. Create a folder in Root Directory which name is  "ShikshaApp".
3. Copy All Json files in this directory.

4. After that open app and input login credentials;
5. Import data in offline mode.
6. Now perform action  "Final In", "Final Out", "Temp In" and "Temp Out" on candidate by input roll no.
7. When all action done need to data export by Clicking "Click here for export data" button.


# Steps for Online
No Additional steps required for online mode.

1. Open app and input login credentials after that click "Login Online";
2. Need to Import data in Online mode by clicking "Import Online".
3. After that Now perform action "Final In", "Final Out", "Temp In" and "Temp Out" on candidate by input roll no.
4. When all action done need to data export by Clicking "Click here for export data" button.