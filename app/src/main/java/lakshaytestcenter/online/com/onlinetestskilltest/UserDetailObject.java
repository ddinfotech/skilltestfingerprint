package lakshaytestcenter.online.com.onlinetestskilltest;

public class UserDetailObject {
    String username;
    String password;

    public UserDetailObject(String username, String password) {
        this.username = username;
        this.password = password;
    }
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
