package lakshaytestcenter.online.com.onlinetestskilltest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pradeep on 11,December,2019
 */
public class CandidateDetailObject implements Parcelable {

    private String rollNo;
    private String detailId;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String dateOfBirth;
    private String address1;
    private String address2;
    private String state;
    private String city;
    private String pincode;
    private String profilePicture;

    // Scanned Biometric detail
    private String fingerId;
    private String fingerString;
    private String candidateFingerImage;
    private String candidateProfileImage;
    private String candidateSignature;
    private String allottedLabSeat;

    public CandidateDetailObject() {

    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFingerId() {
        return fingerId;
    }

    public void setFingerId(String fingerId) {
        this.fingerId = fingerId;
    }

    public String getFingerString() {
        return fingerString;
    }

    public void setFingerString(String fingerString) {
        this.fingerString = fingerString;
    }

    public String getCandidateFingerImage() {
        return candidateFingerImage;
    }

    public void setCandidateFingerImage(String candidateFingerImage) {
        this.candidateFingerImage = candidateFingerImage;
    }

    public String getCandidateProfileImage() {
        return candidateProfileImage;
    }

    public void setCandidateProfileImage(String candidateProfileImage) {
        this.candidateProfileImage = candidateProfileImage;
    }

    public String getCandidateSignature() {
        return candidateSignature;
    }

    public void setCandidateSignature(String candidateSignature) {
        this.candidateSignature = candidateSignature;
    }

    public String getAllottedLabSeat() {
        return allottedLabSeat;
    }

    public void setAllottedLabSeat(String allottedLabSeat) {
        this.allottedLabSeat = allottedLabSeat;
    }

    protected CandidateDetailObject(Parcel in) {
        detailId  = in.readString();
        rollNo = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        fatherName = in.readString();
        dateOfBirth = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        state = in.readString();
        city = in.readString();
        pincode = in.readString();
        profilePicture = in.readString();
    }

    public static final Creator<CandidateDetailObject> CREATOR = new Creator<CandidateDetailObject>() {
        @Override
        public CandidateDetailObject createFromParcel(Parcel in) {
            return new CandidateDetailObject(in);
        }

        @Override
        public CandidateDetailObject[] newArray(int size) {
            return new CandidateDetailObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(detailId);
        dest.writeString(rollNo);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(fatherName);
        dest.writeString(dateOfBirth);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(state);
        dest.writeString(city);
        dest.writeString(pincode);
        dest.writeString(profilePicture);
    }
}
