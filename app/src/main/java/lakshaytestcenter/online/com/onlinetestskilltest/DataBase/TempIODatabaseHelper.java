package lakshaytestcenter.online.com.onlinetestskilltest.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TempIODatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION_TEMPIO = 1;

    // Database Name
    private static final String DATABASE_NAME_TEMPIO = "TempIO_db";


    public TempIODatabaseHelper(Context context) {
        super(context, DATABASE_NAME_TEMPIO, null, DATABASE_VERSION_TEMPIO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create table
        sqLiteDatabase.execSQL(TempIODatabaseModel.CREATE_TABLE_TEMP_IO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldV, int newV) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TempIODatabaseModel.TEMP_IO_TABLE_NAME);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public long insertDataIO(
           String rollno,
            String starttime,
            String endtime,
            int duration,
            String matchflag
    ){
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB, rollno);
        values.put(TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB,starttime);
        values.put(TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB,endtime);
        values.put(TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB,duration);
        values.put(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB,matchflag);
        // insert row
        long id = db.insert(TempIODatabaseModel.TEMP_IO_TABLE_NAME, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;
    }


    public TempIODatabaseModel getTempIODatabaseModel(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TempIODatabaseModel.TEMP_IO_TABLE_NAME,
                new String[]{
                        TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB,
                        TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB,
                        TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB,
                        TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB,
                        TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB,
                        TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB
                },
                TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        TempIODatabaseModel tempIODatabaseModel = new TempIODatabaseModel(
                cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB)),
                cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB)),
                cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB)),
                cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB)),
                cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB)),
                cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB))
        );

        // close the db connection
        cursor.close();

        return tempIODatabaseModel;
    }


    public List<TempIODatabaseModel> getAllIOModels() {
        List<TempIODatabaseModel> models = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TempIODatabaseModel.TEMP_IO_TABLE_NAME + " ORDER BY " +
                TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TempIODatabaseModel ioDatabaseModel = new TempIODatabaseModel();
                ioDatabaseModel.setKeyid(cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB)));
                ioDatabaseModel.setRollno(cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB)));
                ioDatabaseModel.setStarttime(cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB)));
                ioDatabaseModel.setEndtime(cursor.getString(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB)));
                ioDatabaseModel.setDuration(cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB)));
                ioDatabaseModel.setMatchflag(cursor.getInt(cursor.getColumnIndex(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB)));
                models.add(ioDatabaseModel);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return models;
    }
    public int getModelsCount() {
        String countQuery = "SELECT  * FROM " + TempIODatabaseModel.TEMP_IO_TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }
    public int updateIOModel(TempIODatabaseModel model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB, model.getRollno());
        values.put(TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB,model.getStarttime());
        values.put(TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB,model.getEndtime());
        values.put(TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB,model.getDuration());
        values.put(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB,model.getMatchflag());

        // updating row
        return db.update(TempIODatabaseModel.TEMP_IO_TABLE_NAME, values, TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB + " = ?",
                new String[]{String.valueOf(model.getKeyid())});
    }
    public void deleteIOModel(TempIODatabaseModel model) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TempIODatabaseModel.TEMP_IO_TABLE_NAME, TempIODatabaseModel.COlUMN_P_KEY_IO_MODEL_DB + " = ?",
                new String[]{String.valueOf(model.getKeyid())});
        db.close();
    }


    public int updateTempOut(String rollno, String srttime) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TempIODatabaseModel.COLUMN_START_TIME_IO_MODEL_DB,srttime);
        values.put(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB,"match");
        // updating row
        return db.update(TempIODatabaseModel.TEMP_IO_TABLE_NAME, values, TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB + " = ?",
                new String[]{String.valueOf(rollno)});
    }

    public int updateTemplInt(String rollno,String endtime) {
        String startdate= getSingleEntry(String.valueOf(rollno));
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB,endtime);
        values.put(TempIODatabaseModel.COLUMN_MATCH_FLAG_IO_MODEL_DB,"match");
        values.put(TempIODatabaseModel.COLUMN_DURATION_IO_MODEL_DB,getDuration(startdate,endtime));
        // updating row
        return db.update(TempIODatabaseModel.CREATE_TABLE_TEMP_IO, values,
                TempIODatabaseModel.COLUMN_ROLLNO_IO_MODEL_DB + " = "+rollno+" AND "
                        +TempIODatabaseModel.COLUMN_END_TIME_IO_MODEL_DB+" = " + "''",
                new String[]{String.valueOf(rollno)});
    }

    //getting single entry from table  if from direct entry
    public String getSingleEntry(String rollnum) {
        String starttime = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TempIODatabaseModel.TEMP_IO_TABLE_NAME+" WHERE roll_no='" + rollnum + "'",null);
        if (res != null && res.moveToFirst()) {
            do {
                starttime=res.getColumnName(4);
                return starttime;
            }
            while (res.moveToNext());
        }
        return starttime;
    }
    public String getDuration(String arrival_time,String endtime)
    {

        DateFormat format = new SimpleDateFormat("ddMMMyyyyHH:mm");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(String.valueOf(arrival_time));

            date2 = format.parse(String.valueOf(endtime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = date2.getTime() - date1.getTime();
        int seconds = (int) diff;
        int hrs = seconds / (60 * 60);
        int minutes = seconds / 60;
        seconds = seconds % 60;
        String time = hrs + ":" + minutes + ":" + seconds;
        return time;
    }
}
