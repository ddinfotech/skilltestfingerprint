package lakshaytestcenter.online.com.onlinetestskilltest.utilities;

/**
 * Created by ANU on 4/16/2018.
 */

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;

import com.android.volley.DefaultRetryPolicy;

import lakshaytestcenter.online.com.onlinetestskilltest.LoginActivity;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences pref1;

    // Editor for Shared preferences
    Editor editor,editor1;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AndroidHivePref";
    private static final String PREF_NAME_iMPORTD = "iMPORTED";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String ISIMPORTED = "ISIMPORTED";
    private static final String NOTI_TYPE = "both";
    private static final String NOTI_daily = "noti_daily";
    private static final String NOTI_weekly = "noti_weekly";
    private static final String ISEXPORTlOG = "ISEXPORTlOG";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        pref1 = _context.getSharedPreferences(PREF_NAME_iMPORTD, PRIVATE_MODE);
        editor = pref.edit();
        editor1 = pref1.edit();
    }


    public String getNoti_typeD(Context context) {
        return pref.getString(NOTI_TYPE, "null");
    }
    public void set_noti_type_both(Context context, Bitmap both) {

        editor.putString(NOTI_TYPE, String.valueOf(both));
        editor.commit();

    }


    /**
     * Create login session
     */
    public void createLoginSession(String email, String password) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_NAME, email);
        // Storing email in pref
        editor.putString(KEY_EMAIL, password);
        // commit changes
        editor.commit();
    }
    public void createImportData() {
        editor1.putBoolean(ISIMPORTED, true);
        editor1.commit();
    }

    public void exportedLogFile() {
        editor1.putBoolean(ISEXPORTlOG, true);
        editor1.commit();
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        editor1.clear();
        editor1.commit();

        // After logout redirect user to Loing Activity
//        Intent i = new Intent(_context, LoginActivity.class);
//        // Closing all the Activities
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        // Add new Flag to start new Activity
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        // Staring Login Activity
//        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State

    public String getAdminUsername(String key){
        String adminUser = pref.getString(KEY_NAME,"");
        return adminUser;
    }
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
    public boolean ismportedData() {
        return pref1.getBoolean(ISIMPORTED, false);
    }
    public static final String BASE_URL = "http://api.ddinfotech.in/api/";
    public static final String Login = BASE_URL + "adminlogin";
    public static final String Import = BASE_URL + "FingerPrintDetail";
    public boolean isExportedLog(){
        return pref1.getBoolean(ISEXPORTlOG , false);
    }


    public static final DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(60 * 1000, 1, 1.0f);


}





