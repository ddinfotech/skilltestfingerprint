package lakshaytestcenter.online.com.onlinetestskilltest;

public class ImageDetail {
    int rollNumber;
    byte [] oldImage;
    byte [] newImage;

    public ImageDetail(int rollNumber, byte[] oldImage, byte[] newImage) {
        this.rollNumber = rollNumber;
        this.oldImage = oldImage;
        this.newImage = newImage;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public byte[] getOldImage() {
        return oldImage;
    }

    public byte[] getNewImage() {
        return newImage;
    }
}
