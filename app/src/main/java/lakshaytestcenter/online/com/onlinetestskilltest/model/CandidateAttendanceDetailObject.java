package lakshaytestcenter.online.com.onlinetestskilltest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pradeep on 11,December,2019
 */
public class CandidateAttendanceDetailObject implements Parcelable {

    private String rollNo;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String allottedLabSeat;
    private String fingerString;
    private String candidateFingerImage;
    private String candidateProfileImage;
    private String candidateSignature;

    private String capturedFingerString;
    private String capturedFingerImage;
    private String capturedProfileImage;
    private String capturedSignature;
    private String attendanceStatus;
    private String fingerPrintMatchingStatus;


    public CandidateAttendanceDetailObject() {

    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFingerString() {
        return fingerString;
    }

    public void setFingerString(String fingerString) {
        this.fingerString = fingerString;
    }

    public String getCandidateFingerImage() {
        return candidateFingerImage;
    }

    public void setCandidateFingerImage(String candidateFingerImage) {
        this.candidateFingerImage = candidateFingerImage;
    }

    public String getCandidateProfileImage() {
        return candidateProfileImage;
    }

    public void setCandidateProfileImage(String candidateProfileImage) {
        this.candidateProfileImage = candidateProfileImage;
    }

    public String getCandidateSignature() {
        return candidateSignature;
    }

    public void setCandidateSignature(String candidateSignature) {
        this.candidateSignature = candidateSignature;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAllottedLabSeat() {
        return allottedLabSeat;
    }

    public void setAllottedLabSeat(String allottedLabSeat) {
        this.allottedLabSeat = allottedLabSeat;
    }

    public String getCapturedFingerImage() {
        return capturedFingerImage;
    }

    public void setCapturedFingerImage(String capturedFingerImage) {
        this.capturedFingerImage = capturedFingerImage;
    }

    public String getCapturedProfileImage() {
        return capturedProfileImage;
    }

    public void setCapturedProfileImage(String capturedProfileImage) {
        this.capturedProfileImage = capturedProfileImage;
    }

    public String getCapturedSignature() {
        return capturedSignature;
    }

    public void setCapturedSignature(String capturedSignature) {
        this.capturedSignature = capturedSignature;
    }

    public String getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public String getCapturedFingerString() {
        return capturedFingerString;
    }

    public void setCapturedFingerString(String capturedFingerString) {
        this.capturedFingerString = capturedFingerString;
    }

    public String getFingerPrintMatchingStatus() {
        return fingerPrintMatchingStatus;
    }

    public void setFingerPrintMatchingStatus(String fingerPrintMatchingStatus) {
        this.fingerPrintMatchingStatus = fingerPrintMatchingStatus;
    }

    protected CandidateAttendanceDetailObject(Parcel in) {
        rollNo = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        fatherName = in.readString();
        allottedLabSeat = in.readString();
        candidateFingerImage = in.readString();
        candidateProfileImage = in.readString();
        candidateSignature = in.readString();
        attendanceStatus = in.readString();
    }

    public static final Creator<CandidateAttendanceDetailObject> CREATOR = new Creator<CandidateAttendanceDetailObject>() {
        @Override
        public CandidateAttendanceDetailObject createFromParcel(Parcel in) {
            return new CandidateAttendanceDetailObject(in);
        }

        @Override
        public CandidateAttendanceDetailObject[] newArray(int size) {
            return new CandidateAttendanceDetailObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rollNo);
        dest.writeString(firstName);
        dest.writeString(fingerString);
        dest.writeString(candidateFingerImage);
        dest.writeString(candidateProfileImage);
        dest.writeString(candidateSignature);
        dest.writeString(attendanceStatus);

    }
}
