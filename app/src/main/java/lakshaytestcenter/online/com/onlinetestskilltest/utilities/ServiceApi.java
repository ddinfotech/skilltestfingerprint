package lakshaytestcenter.online.com.onlinetestskilltest.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by ANU on 6/19/2018.
 */

public class ServiceApi {
    Context context;
    private ProgressDialog p;

//    public static String localUrl = "http://api.ddinfotech.in/";
//    public static String login = "http://api.ddinfotech.in/api/adminlogin";
//    public static String CandidateData = "http://api.ddmock.com/api/candidatedetail";
//    public static String FingerPrintDetail = "http://api.ddmock.com/api/candidatefinger";
//    public static String exportLog = "http://api.ddmock.com/api/ExportData";
//    public static String exportImage = "http://api.ddmock.com/api/ExportImage";

//    public static String localUrl = "http://cbtskilltestbioapi.ddmock.com/";
//    public static String login = "http://cbtskilltestbioapi.ddmock.com/api/adminlogin";
//    public static String CandidateData = "http://cbtskilltestbioapi.ddmock.com/api/candidatedetail";
//    public static String FingerPrintDetail = "http://cbtskilltestbioapi.ddmock.com/api/candidatefinger";
//    public static String exportLog = "http://cbtskilltestbioapi.ddmock.com/api/ExportData";
//    public static String exportImage = "http://cbtskilltestbioapi.ddmock.com/api/ExportImage";  
    
    public static String localUrl = "http://cbtskilltestbioapi.ddmock.com/";
    public static String login = "http://cbtskilltestbioapi.ddmock.com/api/adminlogin";
    public static String CandidateData = "http://cbtskilltestbioapi.ddmock.com/api/candidatedetail";
//    public static String FingerPrintDetail = "http://cbtskilltestbioapi.ddmock.com/api/candidatefinger";
    public static String FingerPrintDetail = "http://192.168.1.12:8008/api/candidatefinger";
    public static String exportLog = "http://cbtskilltestbioapi.ddmock.com/api/ExportData";
    public static String exportImage = "http://cbtskilltestbioapi.ddmock.com/api/ExportImage";
    public static String EXPORT_ATTENDANCE = "http://192.168.1.12:8008/api/AttendanceList";


    private static final String TAG = ServiceApi.class.getSimpleName();
    SessionManager prefencesManager;

    public ServiceApi(Context context) {
        this.context = context;
        prefencesManager = new SessionManager(context);
    }

    /* ********************** Start Post Request ********************* */

    public String load(String contents, String requesttype) throws IOException {
        Log.d(TAG, requesttype);
        URL url = new URL( requesttype);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(60000);
        conn.setReadTimeout(60000);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("X-KeyRegToken", "v;U?F=Vh9s$xkm!2C5~L:mXn&E[5bNq~,>u?WfF;G");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
        w.write(contents);
        w.flush();
        InputStream istream = conn.getInputStream();
        String result = convertStreamToUTF8String(istream);
        return result;
    }

    private static String convertStreamToUTF8String(InputStream stream)
            throws IOException {
        String result = "";
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[4096];
            int readedChars = 0;
            while (readedChars != -1) {
                readedChars = reader.read(buffer);
                if (readedChars > 0)
                    sb.append(buffer, 0, readedChars);
            }
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /* ********************** End Post Request ********************* */

    /* ********************** Start Get Request ********************* */

    private String loadGet(String urlWithData, String requestType) throws Exception {
        try {
            Log.d(TAG,  urlWithData);
            URL mUrl = new URL(urlWithData);
            HttpURLConnection httpConnection = (HttpURLConnection) mUrl.openConnection();
            httpConnection.setRequestMethod(requestType);
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("X-KeyRegToken", "v;U?F=Vh9s$xkm!2C5~L:mXn&E[5bNq~,>u?WfF;G");
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(60000);
            httpConnection.setReadTimeout(60000);
            httpConnection.connect();
            int responseCode = httpConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                return sb.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Uri buildURI(String url, HashMap<String, String> params) {
        Uri.Builder builder = Uri.parse(localUrl + url).buildUpon();

        if (params != null) {
            for (HashMap.Entry<String, String> entry : params.entrySet()) {
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }


    /* ********************** End Get Request ********************* */


    public String adminLogin(String userID, String password) throws Exception {
        //JSONObject result = null;
        JSONObject o = new JSONObject();
        o.put("userid", userID);
        o.put("password", password);
        String s = o.toString();
        Log.d(TAG, s);
        String r = load(s, login);
        return r;
    }

    public String exportLogFile(String data) throws Exception {
        String s = data;
        Log.d(TAG, s);
        String r = load(s, exportLog);
        return r;
    }

    public String exportImage(String data) throws Exception {
        String s = data;
        Log.d(TAG, s);
        String r = load(s, exportImage);
        return r;
    }

    public String exportAttendance(String data) throws Exception {
        String s = data;
        Log.d(TAG, s);
        String r = load(s, EXPORT_ATTENDANCE);
        return r;
    }

    /***
     * Import Method

     * @return
     * @throws Exception
     */
    public String ImportData() throws Exception {
        String r = loadGet(FingerPrintDetail, "GET");
        return r;
    }

    /***
     * Import Method

     * @return
     * @throws Exception
     */
    public String ImportCanditateData() throws Exception {

        String r =loadGet(CandidateData, "GET");
        return r;
    }


}


