package lakshaytestcenter.online.com.onlinetestskilltest.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lakshaytestcenter.online.com.onlinetestskilltest.ImageDetail;
import lakshaytestcenter.online.com.onlinetestskilltest.LogDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.UserDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.model.CandidateAttendanceDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.model.CandidateDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;


public class FingerDatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION_FP_DB = 1;

    // Database Name
    private static final String DATABASE_NAME_FP_DB = "fingerprints_db";
    public static final String LOG_TABLE = "LOG_TABLE";
    public static final String USER_TABLE = "USER_TABLE";
    public static final String CAPTURE_IMAGE_TABLE = "CAPTURE_IMAGE_TABLE";
    public static final String CANDIDATE_ATTENDANCE_TABLE = "tbl_attendance";
    public static final String CANDIDATE_DETAIL_TABLE = "tbl_candidate_detail";
    public static final String CANDIDATE_BIOMETRIC_DETAIL_TABLE = "tbl_biometric_detail";
    SessionManager sessionManager;

    public interface LOG_COLUMN {
        String ADMIN_USERNAME = "ADMIN_USERNAME";
        String ROLL_NUMBER = "roll_no";
        String DATE = "DATE";
        String ACTIVITY = "Activity";
        String STATUS = "Status";
    }
    public interface USER_COLUMN {
        String USERNAME = "USERNAME";
        String PASSWORD = "PASSWORD";
    }
    public interface IMAGE_COLUMN {
        String ROLL_NUMBER = "roll_no";
        String CANDIDATE_OLD_IMAGE = "CANDIDATE_OLD_IMAGE";
        String CANDIDATE_NEW_IMAGE = "CANDIDATE_NEW_IMAGE";
    }

    public interface CANDIDATE_ATTENDANCE_COLUMN {
        String ROLL_NUMBER = "roll_no";
//        String CANDIDATE_FIRST_NAME = "first_name";
//        String CANDIDATE_LAST_NAME = "last_name";
//        String CANDIDATE_FATHER_NAME = "father_name";
//        String CANDIDATE_ALLOTTED_SEAT = "allotted_seat";
        String CANDIDATE_CAPTURED_IMAGE = "captured_image";
        String CANDIDATE_CAPTURED_FINGER_PRINT = "captured_fingerprint";
        String CANDIDATE_CAPTURED_SIGNATURE = "captured_signature";
        String CANDIDATE_CAPTURED_FP_STATUS = "captured_fp_status";
        String CANDIDATE_CAPTURED_FP_STRING = "captured_fp_string";
        String CANDIDATE_ATTENDANCE_STATUS = "attendance_status";

    }

    public interface CANDIDATE_DETAIL_COLUMN {
        String ROLL_NUMBER = "roll_no";
        String CANDIDATE_DETAIL_ID = "detail_id";
        String CANDIDATE_FIRST_NAME = "first_name";
        String CANDIDATE_LAST_NAME = "last_name";
        String CANDIDATE_FATHER_NAME = "father_name";
        String CANDIDATE_DOB = "date_of_birth";
        String CANDIDATE_ADDRESS1 = "address1";
        String CANDIDATE_ADDRESS2 = "address2";
        String CANDIDATE_STATE = "state";
        String CANDIDATE_CITY = "city";
        String CANDIDATE_PINCODE = "pincode";
        String CANDIDATE_PICTURE = "picture";
    }

    public interface CANDIDATE_BIOMETRIC_DETAIL_COLUMN {
        String CANDIDATE_FINGER_ID = "finger_id";
        String ROLL_NUMBER = "roll_no";
        String CANDIDATE_FINGER_IMAGE = "finger_image";
        String CANDIDATE_FINGER_STRING = "finger_string";
        String CANDIDATE_IMAGE = "candidate_image";
        String CANDIDATE_SIGANATURE = "candidate_signature";
        String CANDIDATE_ALLOTTED_LAB_SEAT = "allotted_lab_seat";
    }

    public FingerDatabaseHelper(Context context) {
        super(context, DATABASE_NAME_FP_DB, null, DATABASE_VERSION_FP_DB);
        sessionManager = new SessionManager(context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FingerDatabaseModel.CREATE_TABLE_FINGER_DB);
        db.execSQL("create table if not exists " + LOG_TABLE + " (" +
                LOG_COLUMN.ADMIN_USERNAME + " varchar , " +
                LOG_COLUMN.ROLL_NUMBER + " INTEGER , " +
                LOG_COLUMN.DATE + " varchar , " +
                LOG_COLUMN.ACTIVITY + " varchar , " +
                LOG_COLUMN.STATUS + " INTEGER )");
        db.execSQL("create table if not exists " + USER_TABLE + " (" +
                USER_COLUMN.USERNAME + " varchar , " +
                USER_COLUMN.PASSWORD + " varchar )");
        db.execSQL("create table if not exists " + CAPTURE_IMAGE_TABLE + " (" +
                IMAGE_COLUMN.ROLL_NUMBER + " INTEGER , " +
                IMAGE_COLUMN.CANDIDATE_OLD_IMAGE + " blob , " +
                IMAGE_COLUMN.CANDIDATE_NEW_IMAGE + " blob )");

        db.execSQL("create table if not exists " + CANDIDATE_ATTENDANCE_TABLE + " (" +
                CANDIDATE_ATTENDANCE_COLUMN.ROLL_NUMBER + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_IMAGE + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FINGER_PRINT + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STRING + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STATUS + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_SIGNATURE + " varchar , " +
                CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_ATTENDANCE_STATUS + " varchar )");

        db.execSQL("create table if not exists " + CANDIDATE_DETAIL_TABLE + " (" +
                CANDIDATE_DETAIL_COLUMN.ROLL_NUMBER + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_DETAIL_ID + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_FIRST_NAME + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_LAST_NAME + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_FATHER_NAME + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_DOB + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_ADDRESS1 + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_ADDRESS2 + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_STATE + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_CITY + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_PINCODE + " varchar , " +
                CANDIDATE_DETAIL_COLUMN.CANDIDATE_PICTURE + " varchar )");


        db.execSQL("create table if not exists " + CANDIDATE_BIOMETRIC_DETAIL_TABLE + " (" +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.ROLL_NUMBER + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_ID + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_STRING + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_IMAGE + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_IMAGE + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_SIGANATURE + " varchar , " +
                CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_ALLOTTED_LAB_SEAT + " varchar )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //drop table if exists
        db.execSQL("DROP TABLE IF EXISTS " + FingerDatabaseModel.FINGER_DB_TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

    public List<FingerDatabaseModel> getAllModels() {
        List<FingerDatabaseModel> models = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + FingerDatabaseModel.FINGER_DB_TABLE_NAME + " ORDER BY " +
                FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                FingerDatabaseModel model = new FingerDatabaseModel();
                model.setRollno(cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB)));
                model.setFingerid(cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB)));
                model.setFingerimage(cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERIMAGE_FINGER_DB)));
                model.setFingerstring(cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERSTRING_FINGER_DB)));
                model.setCandidateImage(cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB)));
                model.setStartDateTime(cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_START_DATE_TIME_FINGER_DB)));
                model.setEndDateTime(cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_END_DATE_TIME_FINGER_DB)));
                model.setStartFlag(cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_START_FLAG_FINGER_DB)));
                model.setEndFlag(cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_END_FLAG_FINGER_DB)));
                model.setDuration(cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_DURATION_FINGER_DB)));
                models.add(model);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return notes list
        return models;
    }

    public synchronized boolean insertUserDetail(ArrayList<UserDetailObject> objectArrayList){
        boolean isUserData = false ;
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            try {
                sqLiteDatabase.beginTransaction();
                if (objectArrayList != null && objectArrayList.size() > 0) {
                    ContentValues contentValues = new ContentValues();
                    for (UserDetailObject object : objectArrayList) {
                        contentValues.put(USER_COLUMN.USERNAME, object.getUsername());
                        contentValues.put(USER_COLUMN.PASSWORD, object.getPassword());
                        try {
                            long l=0;
                           l =  sqLiteDatabase.insert(USER_TABLE, null, contentValues);
                            if (l > 0) {
                                isUserData = true;
                            }
                        }catch (Exception ex){
                            isUserData = false;
                        }
                        contentValues.clear();
                    }
                    sqLiteDatabase.setTransactionSuccessful();
                }
            } finally {
                sqLiteDatabase.endTransaction();
                sqLiteDatabase.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            isUserData = false;
        }
        return isUserData;
    }

    public synchronized boolean insertImage(String rollNumber , byte[] image){
        boolean isOldImage;
        byte[] oldImage = null;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = db.rawQuery("select "+IMAGE_COLUMN.CANDIDATE_OLD_IMAGE+" from " +CAPTURE_IMAGE_TABLE +" WHERE "+IMAGE_COLUMN.ROLL_NUMBER +" =? ", new String[]{rollNumber});
        if (cursor != null && cursor.getCount() > 0) {
            oldImage = cursor.getBlob(cursor.getColumnIndex(IMAGE_COLUMN.CANDIDATE_OLD_IMAGE));
        }
        if (oldImage != null) {
                values.put(IMAGE_COLUMN.CANDIDATE_NEW_IMAGE, image);
                db.update(CAPTURE_IMAGE_TABLE, values, IMAGE_COLUMN.ROLL_NUMBER  + " = ?",
                        new String[]{rollNumber});
            isOldImage = false;
        }else {
            values.put(IMAGE_COLUMN.CANDIDATE_OLD_IMAGE, image);
            db.insert(CAPTURE_IMAGE_TABLE,null,values);
            isOldImage = true;
        }
        values.clear();
        db.close();
        return isOldImage;
    }

    public synchronized boolean getImage(String rollNumber , byte[] image){
        boolean isOldImage;
        byte[] oldImage = null;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = db.rawQuery("select "+IMAGE_COLUMN.CANDIDATE_OLD_IMAGE+" from " +CAPTURE_IMAGE_TABLE +" WHERE "+IMAGE_COLUMN.ROLL_NUMBER +" =? ", new String[]{rollNumber});
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            oldImage = cursor.getBlob(cursor.getColumnIndex(IMAGE_COLUMN.CANDIDATE_OLD_IMAGE));
        }
        if (oldImage != null) {
            values.put(IMAGE_COLUMN.CANDIDATE_NEW_IMAGE, image);
            db.update(CAPTURE_IMAGE_TABLE, values, IMAGE_COLUMN.ROLL_NUMBER  + " = ?",
                    new String[]{rollNumber});
            isOldImage = false;
        }else {
            values.put(IMAGE_COLUMN.CANDIDATE_OLD_IMAGE, image);
            db.insert(CAPTURE_IMAGE_TABLE,null,values);
            isOldImage = true;
        }
        values.clear();
        db.close();
        return isOldImage;
    }



    public synchronized void insertLogDetail(LogDetailObject logObject){
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            try {
                if (logObject != null ) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(LOG_COLUMN.ADMIN_USERNAME,sessionManager.getAdminUsername("name"));
                    contentValues.put(LOG_COLUMN.ROLL_NUMBER,logObject.getRoll_number());
                    contentValues.put(LOG_COLUMN.DATE,logObject.getDate());
                    contentValues.put(LOG_COLUMN.ACTIVITY,logObject.getActivity());
                    contentValues.put(LOG_COLUMN.STATUS,logObject.getStatus());
                    sqLiteDatabase.insert(LOG_TABLE, null, contentValues);
                    System.out.println("Data inserted....");
                    contentValues.clear();
                }
            } finally {
                sqLiteDatabase.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public long insertData(
            int rollno,
            String fingure_id,
            String fingerimage,
            String fingerstring,
            String candidateImage,
            String startDateTime,
            String endDateTime,
            int startFlag,
            int endFlag,
            int duration
    ) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        //finger id not added
        //finger id is on autoincrement
        values.put(FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB, rollno);
        values.put(FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB, fingure_id);
        values.put(FingerDatabaseModel.COLUMN_FINGERSTRING_FINGER_DB, fingerstring);
        values.put(FingerDatabaseModel.COLUMN_FINGERIMAGE_FINGER_DB, fingerimage);
        values.put(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB, candidateImage);
        values.put(FingerDatabaseModel.COLUMN_START_DATE_TIME_FINGER_DB, startDateTime);
        values.put(FingerDatabaseModel.COLUMN_END_DATE_TIME_FINGER_DB, endDateTime);
        values.put(FingerDatabaseModel.COLUMN_START_FLAG_FINGER_DB, startFlag);
        values.put(FingerDatabaseModel.COLUMN_END_FLAG_FINGER_DB, endFlag);
        values.put(FingerDatabaseModel.COLUMN_DURATION_FINGER_DB, duration);

        // insert row
        long id = db.insert(FingerDatabaseModel.FINGER_DB_TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }


    public synchronized ArrayList<LogDetailObject> getAllDataFromLogTable(){
        Cursor data = null;
        LogDetailObject logObject;
        ArrayList<LogDetailObject> logList = new ArrayList<>();
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            data = sqLiteDatabase.rawQuery("select * from "+LOG_TABLE , null);
            while (data.moveToNext()){
                logObject  = new LogDetailObject(data.getInt(1),data.getString(2),data.getString(3),data.getInt(4));
                logList.add(logObject);
            }
            data.close();
            sqLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logList;
    }

    public synchronized ArrayList<ImageDetail> getAllImage(){
        Cursor data = null;
        ImageDetail imageDetail;
        ArrayList<ImageDetail> logList = new ArrayList<>();
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            data = sqLiteDatabase.rawQuery("select * from "+FingerDatabaseModel.FINGER_DB_TABLE_NAME , null);
            while (data.moveToNext()){
                imageDetail  = new ImageDetail(data.getInt(0),data.getBlob(13),data.getBlob(14));
                logList.add(imageDetail);
            }
            data.close();
            sqLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logList;
    }


    public synchronized String getPassword(String username){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor data = sqLiteDatabase.rawQuery("select "+USER_COLUMN.PASSWORD+" from "+ USER_TABLE +" WHERE "+USER_COLUMN.USERNAME+" =?", new String[]{username});
        if (data.getCount() < 1) {
            data.close();
            return "NOT EXIST";
        }
        data.moveToFirst();
        String password = data.getString(data.getColumnIndex(USER_COLUMN.PASSWORD));
        data.close();
        sqLiteDatabase.close();
        return password;
    }


    public FingerDatabaseModel getDatabaseModel(String id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FingerDatabaseModel.FINGER_DB_TABLE_NAME,
                new String[]{
                        FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB,
                        FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB,
                        FingerDatabaseModel.COLUMN_FINGERSTRING_FINGER_DB,
                        FingerDatabaseModel.COLUMN_FINGERIMAGE_FINGER_DB,
                        FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB,
                        FingerDatabaseModel.COLUMN_START_DATE_TIME_FINGER_DB,
                        FingerDatabaseModel.COLUMN_END_DATE_TIME_FINGER_DB,
                        FingerDatabaseModel.COLUMN_START_FLAG_FINGER_DB,
                        FingerDatabaseModel.COLUMN_END_FLAG_FINGER_DB,
                        FingerDatabaseModel.COLUMN_DURATION_FINGER_DB,
                        FingerDatabaseModel.COLUMN_FISTNAME_FINGER_DB,
                        FingerDatabaseModel.COLUMN_LASTNAME_FINGER_DB,
                        FingerDatabaseModel.COLUMN_FATHERNAE_FINGER_DB,
                        FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_NEW,
                        FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD
                },
                FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToFirst();

            // prepare model object
            FingerDatabaseModel model = new FingerDatabaseModel(
                    cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB)),
                    cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERIMAGE_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FINGERSTRING_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_START_DATE_TIME_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_END_DATE_TIME_FINGER_DB)),
                    cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_START_FLAG_FINGER_DB)),
                    cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_END_FLAG_FINGER_DB)),
                    cursor.getInt(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_DURATION_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FISTNAME_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_LASTNAME_FINGER_DB)),
                    cursor.getString(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_FATHERNAE_FINGER_DB)),
                    cursor.getBlob(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_NEW)),
                    cursor.getBlob(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD)));

            // close the db connection
            cursor.close();

            return model;
        } else {
            FingerDatabaseModel model = new FingerDatabaseModel(-1, -1, "", "", "", "", "", 0, 0, 0, "", "", "",null,null);
            cursor.close();
            return model;
        }
    }


    public boolean updateStudentNewImage(byte[] studentNewImage,String  rollNumber){
        boolean isOldImage = false;
        byte[] oldImage = null;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = db.rawQuery("select "+FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD+" from " + FingerDatabaseModel.FINGER_DB_TABLE_NAME +" WHERE "+FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB +" =? ", new String[]{rollNumber});
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()){
                oldImage = cursor.getBlob(cursor.getColumnIndex(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD));
            }
        }
        if (oldImage != null ) {
            if (cursor.moveToFirst()){
                values.put(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_NEW, studentNewImage);
                isOldImage = false;
            }
        }else {
            values.put(FingerDatabaseModel.COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD, studentNewImage);
            isOldImage = true;
        }
        db.update(FingerDatabaseModel.FINGER_DB_TABLE_NAME, values, FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB + " = ?",
                new String[]{rollNumber});
        values.clear();
        db.close();

        return isOldImage;
    }


    public int getDatabaseModelCount() {
        // select all query
        String countQuery = "SELECT  * FROM " + FingerDatabaseModel.FINGER_DB_TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public int updateData(String roll, String fi, String la, String father) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FingerDatabaseModel.COLUMN_FISTNAME_FINGER_DB, fi);
        values.put(FingerDatabaseModel.COLUMN_LASTNAME_FINGER_DB, la);
        values.put(FingerDatabaseModel.COLUMN_FATHERNAE_FINGER_DB, father);
        // updating row
        return db.update(FingerDatabaseModel.FINGER_DB_TABLE_NAME, values, FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB + " = ?",
                new String[]{String.valueOf(roll)});
    }


    public int updateFinalIn(String rollno, String srttime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FingerDatabaseModel.COLUMN_START_DATE_TIME_FINGER_DB, srttime);
        values.put(FingerDatabaseModel.COLUMN_START_FLAG_FINGER_DB, "match");
        // updating row
        return db.update(FingerDatabaseModel.FINGER_DB_TABLE_NAME, values, FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB + " = ?",
                new String[]{String.valueOf(rollno)});
    }

    public int updateFinalout(String rollno, String endtime) {
        String startdate = getSingleEntry(String.valueOf(rollno));
        Log.wtf("timeis33", "fffffffff" + startdate);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FingerDatabaseModel.COLUMN_END_DATE_TIME_FINGER_DB, endtime);
        values.put(FingerDatabaseModel.COLUMN_END_FLAG_FINGER_DB, "match");
        values.put(FingerDatabaseModel.COLUMN_DURATION_FINGER_DB, getDuration(startdate, endtime));
        // updating row
        return db.update(FingerDatabaseModel.FINGER_DB_TABLE_NAME, values, FingerDatabaseModel.COLUMN_ROllNO_FINGER_DB + " = ?",
                new String[]{String.valueOf(rollno)});
    }

    public void deleteData(FingerDatabaseModel dm) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(FingerDatabaseModel.FINGER_DB_TABLE_NAME, FingerDatabaseModel.COLUMN_FINGERID_FINGER_DB + " = ?",
                new String[]{String.valueOf(dm.getFingerid())});
        db.close();
    }

    public String getDuration(String arrival_time, String endtime) {

        DateFormat format = new SimpleDateFormat("ddMMMyyyyHH:mm");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(String.valueOf(arrival_time));

            date2 = format.parse(String.valueOf(endtime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = date2.getTime() - date1.getTime();
        int seconds = (int) diff;
        int hrs = seconds / (60 * 60);
        int minutes = seconds / 60;
        seconds = seconds % 60;
        String time = hrs + ":" + minutes + ":" + seconds;
        return time;
    }

    //getting single entry from table  if from direct entry
    public String getSingleEntry(String rollnum) {
        String starttime = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + FingerDatabaseModel.FINGER_DB_TABLE_NAME + " WHERE rollno='" + rollnum + "'", null);
        if (res != null && res.moveToFirst()) {
            do {
                starttime = res.getColumnName(5);
                Log.wtf("timeis33", "fffffffff" + starttime);
                return starttime;
            }
            while (res.moveToNext());
        }
        return starttime;
    }


    public synchronized void clearTable(String tableName) {
        boolean isCreate = false;
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        try {

            if (sqLiteDatabase == null) {
                sqLiteDatabase = getWritableDatabase();
                isCreate = true;
            }
            ArrayList<String> arrayList = getAllTables();
            if (arrayList.contains(tableName)) {
//                    sqLiteDatabase.delete(tableName, null, null);
                sqLiteDatabase.execSQL("delete from " + tableName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            sqLiteDatabase.close();
        }

////            sqLiteDatabase.delete(tableName, null, null);
//            //    //sqLiteDatabase.close();
//            if (isCreate) {
//                //sqLiteDatabase.close();
//            }
    }

    public synchronized ArrayList<String> getAllTables() {
        ArrayList<String> arrTblNames = new ArrayList<String>();
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();

            try {
                Cursor c = sqLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'  ", null);
                if (c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    do {
                        arrTblNames.add(c.getString(c.getColumnIndex("name")));
                        // c.moveToNext();
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //sqLiteDatabase.close();
        return arrTblNames;
    }

    public synchronized boolean clearAllTables() {
        boolean isClearData = false;
        try {
            ArrayList<String> arrayList = getAllTables();
            for (String table : arrayList) {
                //      db.execSQL(" DROP TABLE IF EXISTS " + table);
                clearTable(table);
            }
            isClearData = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isClearData;
    }

    ///////////////////////////////////////////Pradeep//////////////////////////////////////
    public long insertCandidateDetail(CandidateDetailObject candidateDetailObject) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        //finger id not added
        //finger id is on autoincrement
        values.put(CANDIDATE_DETAIL_COLUMN.ROLL_NUMBER, candidateDetailObject.getRollNo());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_DETAIL_ID, candidateDetailObject.getDetailId());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_FIRST_NAME, candidateDetailObject.getFirstName());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_LAST_NAME, candidateDetailObject.getLastName());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_FATHER_NAME, candidateDetailObject.getFatherName());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_DOB, candidateDetailObject.getDateOfBirth());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_ADDRESS1, candidateDetailObject.getAddress1());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_ADDRESS2, candidateDetailObject.getAddress2());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_STATE, candidateDetailObject.getState());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_CITY, candidateDetailObject.getCity());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_PINCODE, candidateDetailObject.getPincode());
        values.put(CANDIDATE_DETAIL_COLUMN.CANDIDATE_PICTURE, candidateDetailObject.getProfilePicture());

        // insert row
        long id = db.insert(CANDIDATE_DETAIL_TABLE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long insertBiometricDetail(CandidateDetailObject candidateBioDetailObject) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.ROLL_NUMBER, candidateBioDetailObject.getRollNo());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_ID, candidateBioDetailObject.getFingerId());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_IMAGE, candidateBioDetailObject.getCandidateFingerImage());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_STRING, candidateBioDetailObject.getFingerString());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_IMAGE, candidateBioDetailObject.getCandidateProfileImage());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_SIGANATURE, candidateBioDetailObject.getCandidateSignature());
        values.put(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_ALLOTTED_LAB_SEAT, candidateBioDetailObject.getAllottedLabSeat());

        // insert row
        long id = db.insert(CANDIDATE_BIOMETRIC_DETAIL_TABLE, null, values);

        // close db connection
        db.close();

        return id;
    }

    public synchronized long insertCandidateAttendance(CandidateAttendanceDetailObject candidateBioDetailObject) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CANDIDATE_ATTENDANCE_COLUMN.ROLL_NUMBER, candidateBioDetailObject.getRollNo());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_IMAGE, candidateBioDetailObject.getCapturedProfileImage());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FINGER_PRINT, candidateBioDetailObject.getCapturedFingerImage());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_SIGNATURE, candidateBioDetailObject.getCapturedSignature());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STRING, candidateBioDetailObject.getCapturedFingerString());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STATUS, candidateBioDetailObject.getFingerPrintMatchingStatus());
        values.put(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_ATTENDANCE_STATUS, candidateBioDetailObject.getAttendanceStatus());

        Cursor cursor = db.rawQuery("SELECT * FROM " + CANDIDATE_ATTENDANCE_TABLE + " WHERE " + CANDIDATE_ATTENDANCE_COLUMN.ROLL_NUMBER + " =? ", new String[]{candidateBioDetailObject.getRollNo()});
        long ll =-1;
        if (cursor != null && cursor.getCount() > 0) {
            ll = db.update(CANDIDATE_ATTENDANCE_TABLE, values, CANDIDATE_ATTENDANCE_COLUMN.ROLL_NUMBER + " =?", new String[]{candidateBioDetailObject.getRollNo()});
            System.out.println("Attendance updated...." + ll);
        } else {
            ll = db.insert(CANDIDATE_ATTENDANCE_TABLE, null, values);
            System.out.println("Exam Data inserted...."+ ll);
        }

        // close db connection
        db.close();

        return ll;
    }

    public synchronized CandidateAttendanceDetailObject getAttendendanceByRollNo(String rollNo) {
        CandidateAttendanceDetailObject taDetailObject = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();

            cursor = sqLiteDatabase.rawQuery(attendanceQuery(), new String[]{rollNo});

            if (cursor.getCount() < 1) {
                cursor.close();
                return null;
            }
            cursor.moveToFirst();
            taDetailObject = getAttendanceObject(cursor);

            cursor.close();
            sqLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taDetailObject;
    }

    public String attendanceQuery() {

        String q ="SELECT tbl_candidate_detail.first_name,tbl_candidate_detail.last_name," +
                "tbl_candidate_detail.father_name,tbl_candidate_detail.roll_no," +
                "tbl_biometric_detail.finger_string,tbl_biometric_detail.finger_image,tbl_biometric_detail.candidate_image," +
                "tbl_biometric_detail.candidate_signature,tbl_biometric_detail.allotted_lab_seat," +
                "tbl_attendance.captured_image,tbl_attendance.captured_fingerprint,tbl_attendance.captured_signature,tbl_attendance.attendance_status," +
                "tbl_attendance.captured_fp_status,tbl_attendance.captured_fp_string " +
                "FROM tbl_candidate_detail JOIN tbl_biometric_detail ON tbl_candidate_detail.roll_no=tbl_biometric_detail.roll_no " +
                "LEFT JOIN tbl_attendance ON tbl_attendance.roll_no=tbl_candidate_detail.roll_no WHERE tbl_candidate_detail.roll_no = ?";

        return q;
    }

    public synchronized CandidateAttendanceDetailObject getAttendanceObject(Cursor cursor) {
        CandidateAttendanceDetailObject obj = new CandidateAttendanceDetailObject();
        obj.setRollNo(cursor.getString(cursor.getColumnIndex(CANDIDATE_DETAIL_COLUMN.ROLL_NUMBER)));
        obj.setFirstName(cursor.getString(cursor.getColumnIndex(CANDIDATE_DETAIL_COLUMN.CANDIDATE_FIRST_NAME)));
        obj.setLastName(cursor.getString(cursor.getColumnIndex(CANDIDATE_DETAIL_COLUMN.CANDIDATE_LAST_NAME)));
        obj.setFatherName(cursor.getString(cursor.getColumnIndex(CANDIDATE_DETAIL_COLUMN.CANDIDATE_FATHER_NAME)));

        obj.setFingerString(cursor.getString(cursor.getColumnIndex(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_STRING)));
        obj.setCandidateFingerImage(cursor.getString(cursor.getColumnIndex(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_FINGER_IMAGE)));
        obj.setCandidateProfileImage(cursor.getString(cursor.getColumnIndex(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_IMAGE)));
        obj.setCandidateSignature(cursor.getString(cursor.getColumnIndex(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_SIGANATURE)));
        obj.setAllottedLabSeat(cursor.getString(cursor.getColumnIndex(CANDIDATE_BIOMETRIC_DETAIL_COLUMN.CANDIDATE_ALLOTTED_LAB_SEAT)));

        obj.setCapturedProfileImage(cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_IMAGE)));
        obj.setCapturedFingerImage(cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FINGER_PRINT)));
        obj.setCapturedFingerString(cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STRING)));
        obj.setFingerPrintMatchingStatus(cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STATUS)));
        obj.setCapturedSignature(cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_SIGNATURE)));
        String st = cursor.getString(cursor.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_ATTENDANCE_STATUS));
        if (st==null)
            st ="A";
        obj.setAttendanceStatus(st);
        return obj;
    }


    public synchronized ArrayList<CandidateAttendanceDetailObject> getAllAttendanceList(){
        Cursor data = null;
        ArrayList<CandidateAttendanceDetailObject> candidateAttendanceList = new ArrayList<>();
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            data = sqLiteDatabase.rawQuery("select * from "+CANDIDATE_ATTENDANCE_TABLE , null);
            while (data.moveToNext()){
                CandidateAttendanceDetailObject attendanceDetail = new CandidateAttendanceDetailObject();
                attendanceDetail.setRollNo(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.ROLL_NUMBER)));
                attendanceDetail.setCapturedProfileImage(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_IMAGE)));
                attendanceDetail.setCapturedFingerImage(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FINGER_PRINT)));
                attendanceDetail.setCapturedSignature(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_SIGNATURE)));
                attendanceDetail.setCapturedFingerString(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STRING)));
                attendanceDetail.setFingerPrintMatchingStatus(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_CAPTURED_FP_STATUS)));
                attendanceDetail.setAttendanceStatus(data.getString(data.getColumnIndex(CANDIDATE_ATTENDANCE_COLUMN.CANDIDATE_ATTENDANCE_STATUS)));
                candidateAttendanceList.add(attendanceDetail);
            }
            data.close();
            sqLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return candidateAttendanceList;
    }


    public synchronized String getTotalCandidateCount(){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
//        Cursor data = sqLiteDatabase.rawQuery("SELECT COUNT(*) FROM "+CANDIDATE_ATTENDANCE_TABLE , null);
        Cursor data = sqLiteDatabase.rawQuery("select * from "+CANDIDATE_DETAIL_TABLE , null);
        String rowCount = String.valueOf(data.getCount());
        data.close();
        return rowCount;
    }

    public synchronized String getMappedCandidateCount(){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor data = sqLiteDatabase.rawQuery("select * from "+CANDIDATE_BIOMETRIC_DETAIL_TABLE , null);
        String rowCount = String.valueOf(data.getCount());
        data.close();
        return rowCount;
    }
    public synchronized String getPresentCandidateCount(){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor data = sqLiteDatabase.rawQuery("select * from "+CANDIDATE_ATTENDANCE_TABLE , null);
        String rowCount = String.valueOf(data.getCount());
        data.close();
        return rowCount;
    }

    ///////////////////////////////////////// End Task ////////////////////////////////////
}

