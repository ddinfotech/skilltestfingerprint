package lakshaytestcenter.online.com.onlinetestskilltest.DataBase;

public class TempIODatabaseModel {
    public static final String TEMP_IO_TABLE_NAME = "temp_io_db";

    public static final String COlUMN_P_KEY_IO_MODEL_DB = "id";
    public static final String COLUMN_ROLLNO_IO_MODEL_DB ="roll_no";
    public static final String COLUMN_START_TIME_IO_MODEL_DB = "starttime";
    public static final String COLUMN_END_TIME_IO_MODEL_DB = "endtime";
    public static final String COLUMN_DURATION_IO_MODEL_DB = "duration";
    public static final String COLUMN_MATCH_FLAG_IO_MODEL_DB = "matchflag";

    private int keyid;
    private String starttime,endtime;
    private int duration;
    private String rollno;
    private int matchflag;

    public static final String CREATE_TABLE_TEMP_IO = "CREATE TABLE " +
            TEMP_IO_TABLE_NAME + "("
            + COlUMN_P_KEY_IO_MODEL_DB + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_ROLLNO_IO_MODEL_DB + " TEXT,"
            + COLUMN_START_TIME_IO_MODEL_DB +" TEXT,"
            + COLUMN_END_TIME_IO_MODEL_DB + " TEXT,"
            + COLUMN_DURATION_IO_MODEL_DB + " INTEGER,"
            + COLUMN_MATCH_FLAG_IO_MODEL_DB + " INTEGER"
            + ")";

    public TempIODatabaseModel(){
    }
    public TempIODatabaseModel(int keyid, String rollno, String starttime, String endtime, int duration, int matchflag){
        this.keyid =keyid;
        this.rollno = rollno;
        this.starttime= starttime;
        this.endtime = endtime;
        this.duration = duration;
        this.matchflag=matchflag;
    }


    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMatchflag() {
        return matchflag;
    }

    public void setMatchflag(int matchflag) {
        this.matchflag = matchflag;
    }

    public int getKeyid() {
        return keyid;
    }

    public void setKeyid(int keyid) {
        this.keyid = keyid;
    }
}
