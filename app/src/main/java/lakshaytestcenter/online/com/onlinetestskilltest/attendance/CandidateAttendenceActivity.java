package lakshaytestcenter.online.com.onlinetestskilltest.attendance;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.mantra.mfs100.FingerData;
import com.mantra.mfs100.MFS100;
import com.mantra.mfs100.MFS100Event;


import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.R;
import lakshaytestcenter.online.com.onlinetestskilltest.model.CandidateAttendanceDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.UtilManager;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.ZoomableImageView;

public class CandidateAttendenceActivity extends AppCompatActivity implements View.OnClickListener, MFS100Event {

    TextView tvRollnoInput;
    TextView txtEventLog, tvMatchingStatus;
    EditText etFirstNameInput, etLastNameInput, etFatherNameInput, etAllottedLab, etAllottedSeat;
    Button btnCapture, btnCaptureImage, btnCaptureSignature, btnSubmitAttendance;
    LinearLayout llButtonContainer;
    AlertDialog dialog_signature;
    ImageView ivCandidateProfile, ivCapturedProfile, ivCandidateSignature, ivCapturedSignature;
    ImageView ivCapturedFingerPrint, fmFingerPrintImageView;

    FingerDatabaseHelper db;
    String roll_no, finalText;
    public static String candidateImage,fingerstring,fingureImage;
    int timeout = 10000;
    MFS100 mfs100 = null;
    public static FingerData lastCapFingerData = null;
    private boolean isCaptureRunning = false;
    ScannerAction scannerAction = ScannerAction.Capture;
    private enum ScannerAction {
        Capture, Verify
    }

    Bitmap bitmapCapturedImage = null, bitmapCapturedSignature = null ,bitmapCapturedFingerPrint = null;
    String fingerPrintMatchingStatus = "", capturedFPString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_attendence);
        setToolbar();
        initClass();
        initView();
        initGetData();
    }


    @Override
    protected void onStart() {
        if (mfs100 == null) {
            mfs100 = new MFS100(this);
            mfs100.SetApplicationContext(CandidateAttendenceActivity.this);
        } else {
            InitScanner();
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (mfs100 == null) {
            mfs100 = new MFS100(this);
            mfs100.SetApplicationContext(CandidateAttendenceActivity.this);
        } else {
            InitScanner();
        }
        super.onResume();
    }

    private void initClass(){
        db = new FingerDatabaseHelper(this);
        Bundle i = getIntent().getExtras();
        if (i != null) {
            roll_no = i.getString("roll");
            finalText = i.getString("final");

        } else {
            Toast.makeText(getApplicationContext(), "result not found", Toast.LENGTH_LONG).show();
        }

    }

    public void initView(){
        ivCandidateProfile = findViewById(R.id.ivCandidateProfile);
        ivCapturedProfile = findViewById(R.id.ivCapturedProfile);
        ivCandidateSignature = findViewById(R.id.ivCandidateSignature);
        ivCapturedSignature = findViewById(R.id.ivCapturedSignature);

        tvRollnoInput = findViewById(R.id.tvRollnoInput);
        etFirstNameInput = findViewById(R.id.etFirstNameInput);
        etLastNameInput = findViewById(R.id.etLastNameInput);
        etFatherNameInput = findViewById(R.id.etFatherNameInput);
        etAllottedLab = findViewById(R.id.etAllottedLab);
        etAllottedSeat = findViewById(R.id.etAllottedSeat);

        ivCapturedFingerPrint = findViewById(R.id.ivCapturedFingerPrint);
        fmFingerPrintImageView = findViewById(R.id.fmFingerPrintImageView);
        txtEventLog = (TextView) findViewById(R.id.txtEventLog);
        tvMatchingStatus = (TextView) findViewById(R.id.tvMatchingStatus);


        llButtonContainer = findViewById(R.id.llButtonContainer);
        btnCapture = findViewById(R.id.btnCapture);
        btnCaptureImage = findViewById(R.id.btnCaptureImage);
        btnCaptureSignature = findViewById(R.id.btnCaptureSignature);
        btnSubmitAttendance = findViewById(R.id.btnSubmitAttendance);
        btnCapture.setOnClickListener(this);
        btnCaptureImage.setOnClickListener(this);
        btnCaptureSignature.setOnClickListener(this);
        btnSubmitAttendance.setOnClickListener(this);

        ivCandidateProfile.setOnClickListener(this);
        ivCapturedProfile.setOnClickListener(this);
        ivCandidateSignature.setOnClickListener(this);
        ivCapturedSignature.setOnClickListener(this);
    }

    public void setToolbar() {
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void initGetData(){

        CandidateAttendanceDetailObject databaseModel=db.getAttendendanceByRollNo(roll_no);
        if (databaseModel!=null && databaseModel.getRollNo() !=null ) {
            String first_name= databaseModel.getFirstName();
            String last_name= databaseModel.getLastName();
            String father_name= databaseModel.getFatherName();

            fingerstring = databaseModel.getFingerString();
            tvRollnoInput.setText("Roll No is:  " + roll_no);
            etFirstNameInput.setText(getEmptyFromNull(first_name));
            etLastNameInput.setText(getEmptyFromNull(last_name));
            etFatherNameInput.setText(getEmptyFromNull(father_name));

            // Get LAB And Seat by split, First element LAB And second one is SEAT.
            String []allottedLabSeat = databaseModel.getAllottedLabSeat().split("-");
            etAllottedLab.setText(getEmptyFromNull(allottedLabSeat[0]));
            if (allottedLabSeat.length==2){
                etAllottedSeat.setText(getEmptyFromNull(allottedLabSeat[1]));
            }

            ivCandidateProfile.setImageBitmap(UtilManager.getBitmpapFromBase64String(databaseModel.getCandidateProfileImage()));
            ivCandidateSignature.setImageBitmap(UtilManager.getBitmpapFromBase64String(databaseModel.getCandidateSignature()));
            fmFingerPrintImageView.setImageBitmap(UtilManager.getBitmpapFromBase64String(databaseModel.getCandidateFingerImage()));

            if (databaseModel.getAttendanceStatus().equalsIgnoreCase("P")){
                bitmapCapturedImage = UtilManager.loadImageFromStorage(databaseModel.getCapturedProfileImage());
                ivCapturedProfile.setImageBitmap(bitmapCapturedImage);
                bitmapCapturedSignature = UtilManager.loadImageFromStorage(databaseModel.getCapturedSignature());
                ivCapturedSignature.setImageBitmap(bitmapCapturedSignature);
                bitmapCapturedFingerPrint = UtilManager.loadImageFromStorage(databaseModel.getCapturedFingerImage());
                tvMatchingStatus.setText(databaseModel.getFingerPrintMatchingStatus());
                ivCapturedFingerPrint.setImageBitmap(bitmapCapturedFingerPrint);
                llButtonContainer.setVisibility(View.GONE);
                tvMatchingStatus.setVisibility(View.VISIBLE);
                txtEventLog.setVisibility(View.GONE);
            } else {
                tvMatchingStatus.setVisibility(View.GONE);
                txtEventLog.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public void onClick(View v) {

        Bitmap bitmap = null;
        switch (v.getId()){
            case R.id.btnCaptureSignature:
                getSignature(0, ivCapturedSignature);
                break;
            case R.id.btnCapture:
                scannerAction = ScannerAction.Capture;
                if (!isCaptureRunning) {
                    StartSyncCapture();
                }
                break;
            case R.id.btnCaptureImage:
                cameraIntent();
                break;
            case R.id.btnSubmitAttendance:
                if (bitmapCapturedImage==null){
                    Toast.makeText(this, "Please select Candidate Image", Toast.LENGTH_SHORT).show();
                } else if (bitmapCapturedSignature==null){
                    Toast.makeText(this, "Please select Candidate Signature", Toast.LENGTH_SHORT).show();
                } else if (bitmapCapturedFingerPrint==null){
                    Toast.makeText(this, "Please capture Candidate Finger Print", Toast.LENGTH_SHORT).show();
                } else {
                    String postfix =roll_no+".png";
                    String profileImageName = "profile"+postfix;

                    String profileImagePath = UtilManager.saveToInternalStorage(this, UtilManager.ATTENDANCE_DIRECTORY,
                            bitmapCapturedImage, profileImageName);

                    String signatureImageName = "sign"+postfix;
                    String signatureImagePath = UtilManager.saveToInternalStorage(this, UtilManager.ATTENDANCE_DIRECTORY,
                            bitmapCapturedSignature, signatureImageName);

                    String fingerPrintImageName = "fp"+postfix;
                    String fingerPrintImagePath = UtilManager.saveToInternalStorage(this, UtilManager.ATTENDANCE_DIRECTORY,
                            bitmapCapturedFingerPrint, fingerPrintImageName);

                    CandidateAttendanceDetailObject candidateAttendanceDetailObject = new CandidateAttendanceDetailObject();
                    candidateAttendanceDetailObject.setRollNo(roll_no);
                    candidateAttendanceDetailObject.setCapturedProfileImage(profileImagePath);
                    candidateAttendanceDetailObject.setCapturedSignature(signatureImagePath);
                    candidateAttendanceDetailObject.setCapturedFingerImage(fingerPrintImagePath);
                    candidateAttendanceDetailObject.setCapturedFingerString(capturedFPString);
                    candidateAttendanceDetailObject.setFingerPrintMatchingStatus(fingerPrintMatchingStatus);
                    candidateAttendanceDetailObject.setAttendanceStatus("P");

                    if (db.insertCandidateAttendance(candidateAttendanceDetailObject)> 0){
                        Toast.makeText(this, "Attendance Save", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, "Attendance error", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.ivCandidateProfile:
                bitmap = UtilManager.getBitmpapFromBase64String(db.getAttendendanceByRollNo(roll_no).getCandidateProfileImage());
                zoomImageWithBitmap(bitmap);
                break;
            case R.id.ivCapturedProfile:
//                bitmap = UtilManager.getBitmpapFromBase64String(db.getAttendendanceByRollNo(roll_no).getCapturedProfileImage());
                zoomImageWithBitmap(bitmapCapturedImage);
                break;
            case R.id.ivCandidateSignature:
                bitmap = UtilManager.getBitmpapFromBase64String(db.getAttendendanceByRollNo(roll_no).getCandidateSignature());
                zoomImageWithBitmap(bitmap);
                break;
            case R.id.ivCapturedSignature:
//                bitmap = UtilManager.getBitmpapFromBase64String(db.getAttendendanceByRollNo(roll_no).getCapturedSignature());
                zoomImageWithBitmap(bitmapCapturedSignature);
                break;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100){
            if (resultCode == Activity.RESULT_OK) {
                onCaptureImageResult(data);
            }
        }
    }


    private void InitScanner() {
        try {
            int ret = mfs100.Init();
            if (ret != 0) {
                SetTextOnUIThread(mfs100.GetErrorMsg(ret));
            } else {
                SetTextOnUIThread("Init success");
                String info = "Serial: " + mfs100.GetDeviceInfo().SerialNo()
                        + " Make: " + mfs100.GetDeviceInfo().Make()
                        + " Model: " + mfs100.GetDeviceInfo().Model()
                        + "\nCertificate: " + mfs100.GetCertification();

                SetLogOnUIThread(info);

            }
        } catch (Exception ex) {

            SetTextOnUIThread("Init failed, unhandled exception");
        }
    }

    private void StartSyncCapture() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                SetTextOnUIThread("");
                isCaptureRunning = true;
                try {
                    FingerData fingerData = new FingerData();
                    int ret = mfs100.AutoCapture(fingerData, timeout, false);
                    Log.e("StartSyncCapture.RET", "" + ret);
                    if (ret != 0) {
                        SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                    } else {
                        lastCapFingerData = fingerData;
//                        final Bitmap bitmap = BitmapFactory.decodeByteArray(fingerData.FingerImage(), 0,
//                                fingerData.FingerImage().length);
                        bitmapCapturedFingerPrint = BitmapFactory.decodeByteArray(fingerData.FingerImage(), 0,
                                fingerData.FingerImage().length);
                        CandidateAttendenceActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivCapturedFingerPrint.setImageBitmap(bitmapCapturedFingerPrint);
                            }
                        });

                        SetTextOnUIThread("Capture Success");
                        String log = "\nQuality: " + fingerData.Quality()
                                + "\nNFIQ: " + fingerData.Nfiq()
                                + "\nWSQ Compress Ratio: "
                                + fingerData.WSQCompressRatio()
                                + "\nImage Dimensions (inch): "
                                + fingerData.InWidth() + "\" X "
                                + fingerData.InHeight() + "\""
                                + "\nImage Area (inch): " + fingerData.InArea()
                                + "\"" + "\nResolution (dpi/ppi): "
                                + fingerData.Resolution() + "\nGray Scale: "
                                + fingerData.GrayScale() + "\nBits Per Pixal: "
                                + fingerData.Bpp() + "\nWSQ Info: "
                                + fingerData.WSQInfo();
                        SetLogOnUIThread(log);
                        //  SetData2(fingerData);
                        // ExtractANSITemplate1(fingerstring);
                        matchCode(ExtractANSITemplate());
                    }
                } catch (Exception ex) {
                    SetTextOnUIThread("Error333"+ex);
                } finally {
                    isCaptureRunning = false;
                }
            }
        }).start();
    }

    private void SetTextOnUIThread(final String str) {

        txtEventLog.post(new Runnable() {
            public void run() {
                txtEventLog.setText(str);
                Log.wtf("response error", str);
            }
        });
    }

    private void SetLogOnUIThread(final String str) {

        txtEventLog.post(new Runnable() {
            public void run() {
                txtEventLog.append("\n" + str);
            }
        });
    }

    public void matchCode( byte[] ANSITemplate1) {
        capturedFPString = Base64.encodeToString(ANSITemplate1, Base64.NO_WRAP);
        byte[] data = Base64.decode(fingerstring, Base64.DEFAULT);
        int ret = mfs100.MatchANSI(ANSITemplate1, data);
        if (ret >= 0) {
            String currentDate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
            if (ret >= 4000) {
                fingerPrintMatchingStatus = "MATCHED";
                SetTextOnUIThread("Fingerprint matched" );
            } else {
                fingerPrintMatchingStatus = "MISMATCHED";
                SetTextOnUIThread("Fingerprint not matched");
           }
        } else SetTextOnUIThread(mfs100.GetErrorMsg(ret));
    }

    private   byte[] ExtractANSITemplate() {
        try {
            if (lastCapFingerData == null) {
                SetTextOnUIThread("Finger not capture");
                return null;
            }
            byte[] tempData = new byte[2000]; // length 2000 is mandatory
            byte[] ansiTemplate;
            int dataLen = mfs100.ExtractANSITemplate(lastCapFingerData.RawData(), tempData);
            if (dataLen <= 0) {
                if (dataLen == 0) {
                    SetTextOnUIThread("Failed to extract ANSI Template");
                } else {
                    SetTextOnUIThread(mfs100.GetErrorMsg(dataLen));
                }
            } else {
                ansiTemplate = new byte[dataLen];
                System.arraycopy(tempData, 0, ansiTemplate, 0, dataLen);
                //  WriteFile("ANSITemplate.ansi", ansiTemplate);

                SetTextOnUIThread("Extract ANSI Template Success");
                return  ansiTemplate;
            }
        } catch (Exception e) {
            Log.e("Error", "Extract ANSI Template Error", e);
        }
        return  null;
    }


    public void getSignature(final int pos, final ImageView ivSignature) {
        final SignaturePad signaturePad;
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_signature, null);
        alertBuilder.setView(view);
        dialog_signature = alertBuilder.create();
        dialog_signature.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_signature.setCancelable(true);
        // ImageView ivAlertCancel2 = view.findViewById(R.id.ivAlertCancel2);
        signaturePad = view.findViewById(R.id.signaturePad);
        final Button saveButton = view.findViewById(R.id.btn_send);
        final Button clearButton = view.findViewById(R.id.btn_open);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Log.d("","");
            }

            @Override
            public void onSigned() {
                saveButton.setEnabled(true);
                clearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                saveButton.setEnabled(false);
                clearButton.setEnabled(false);
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 bitmapCapturedSignature = signaturePad.getSignatureBitmap();
                ivSignature.setImageBitmap(bitmapCapturedSignature);
                dialog_signature.dismiss();

            }

        });
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();

            }
        });
        dialog_signature.show();
    }


    private String getEmptyFromNull(String stringValue){

        String outString = stringValue;

        if (stringValue.equalsIgnoreCase("null")) {
            outString = "";
        }

        return outString;
    }

    private void cameraIntent(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
            }else {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 100);
            }

        }else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 100);
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmapCapturedImage = (Bitmap) data.getExtras().get("data");
//        Bitmap bitmapSelected = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmapCapturedImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        byte [] image = bytes.toByteArray();
//        boolean isOldImage = db.updateStudentNewImage(image,roll_no);
        boolean isOldImage = false;

        if (isOldImage) {
            ivCapturedProfile.setImageBitmap(bitmapCapturedImage);
        } else {
            ivCapturedProfile.setImageBitmap(bitmapCapturedImage);
        }
//        String currentDate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
//        LogDetailObject logDetailObject = new LogDetailObject(Integer.parseInt(roll_no),currentDate,"Capture Candidate Image for "+finalText,1);
//        db.insertLogDetail(logDetailObject);

    }


    public void zoomImageWithBitmap(Bitmap imageName) {
        androidx.appcompat.app.AlertDialog.Builder builder=new androidx.appcompat.app.AlertDialog.Builder(this);
        View alertView= LayoutInflater.from(this).inflate(R.layout.layout_zoom_imageview,null);
        builder.setView(alertView);
        final androidx.appcompat.app.AlertDialog alertDialog=builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ZoomableImageView ivAlertZoom=(ZoomableImageView)alertView.findViewById(R.id.ivAlertZoom);
        ivAlertZoom.setVisibility(View.GONE);
        ImageView ivZoomImage=(ImageView)alertView.findViewById(R.id.ivZoomImage);
        ivZoomImage.setVisibility(View.VISIBLE);
        ivZoomImage.setImageBitmap(imageName);
        ImageView tvAlertCancel= alertView.findViewById(R.id.tvAlertCancel);
//        tvAlertCancel.setTypeface(tvFontIcon);
        tvAlertCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void showSuccessLog(String key) {
        SetTextOnUIThread("Init success");
        String info = "\nKey: " + key + "\nSerial: "
                + mfs100.GetDeviceInfo().SerialNo() + " Make: "
                + mfs100.GetDeviceInfo().Make() + " Model: "
                + mfs100.GetDeviceInfo().Model()
                + "\nCertificate: " + mfs100.GetCertification();
        SetLogOnUIThread(info);
    }

    private void UnInitScanner() {
        try {
            int ret = mfs100.UnInit();
            if (ret != 0) {
                SetTextOnUIThread(mfs100.GetErrorMsg(ret));
            } else {
                SetLogOnUIThread("Uninit Success");
                SetTextOnUIThread("Uninit Success");
                lastCapFingerData = null;
            }
        } catch (Exception e) {
            Log.e("UnInitScanner.EX", e.toString());
        }
    }

    @Override
    public void OnDeviceAttached(int vid, int pid, boolean hasPermission) {
        int ret;
        if (!hasPermission) {
            SetTextOnUIThread("Permission denied");
            return;
        }
        if (vid == 1204 || vid == 11279) {
            if (pid == 34323) {
                ret = mfs100.LoadFirmware();
                if (ret != 0) {
                    SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                } else {
                    SetTextOnUIThread("Load firmware success");
                }
            } else if (pid == 4101) {
                String key = "Without Key";
                ret = mfs100.Init();
                if (ret == 0) {
                    showSuccessLog(key);
                } else {
                    SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                }

            }
        }
    }

    @Override
    public void OnDeviceDetached() {
        UnInitScanner();
        SetTextOnUIThread("Device removed");
    }

    @Override
    public void OnHostCheckFailed(String err) {
        try {
            SetLogOnUIThread(err);
            Toast.makeText(this, err, Toast.LENGTH_LONG).show();
        } catch (Exception ignored) {
        }
    }


}
