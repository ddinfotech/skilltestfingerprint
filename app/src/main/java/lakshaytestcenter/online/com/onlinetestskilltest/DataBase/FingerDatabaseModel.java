package lakshaytestcenter.online.com.onlinetestskilltest.DataBase;

public class FingerDatabaseModel {
    public static final String FINGER_DB_TABLE_NAME = "candidate_finger_db";

    public static final String COLUMN_FINGERID_FINGER_DB = "fingerid";
    public static final String COLUMN_ROllNO_FINGER_DB = "rollno";
    public static final String COLUMN_FINGERIMAGE_FINGER_DB = "fingerimage";
    public static final String COLUMN_FINGERSTRING_FINGER_DB = "fingerstring";
    public static final String COLUMN_CANDIDATEIMAGE_FINGER_DB = "candidateImage";
    public static final String COLUMN_START_DATE_TIME_FINGER_DB = "start_datetime";
    public static final String COLUMN_END_DATE_TIME_FINGER_DB = "end_datetime";
    public static final String COLUMN_START_FLAG_FINGER_DB = "start_flag";
    public static final String COLUMN_END_FLAG_FINGER_DB = "end_flag";
    public static final String COLUMN_DURATION_FINGER_DB = "duration";
    public static final String COLUMN_FISTNAME_FINGER_DB = "first_name";
    public static final String COLUMN_LASTNAME_FINGER_DB = "last_name";
    public static final String COLUMN_FATHERNAE_FINGER_DB = "fathe_name";
    public static final String COLUMN_CANDIDATEIMAGE_FINGER_DB_NEW = "candidateImageNew";
    public static final String COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD = "candidateImageOld";


    private int fingerid;
    private int rollno;
    private String fingerimage;
    private String fingerstring;
    private String candidateImage;
    private String startDateTime;
    private String endDateTime;
    private int startFlag;
    private int endFlag;
    private int duration;
    private String first_name;
    private String last_name;
    private String fathe_name;
    private byte [] studentNewImage;
    private byte[] studentOldImage;

    public FingerDatabaseModel() {
    }

    public FingerDatabaseModel(int fingerid, int rollno, String fingerimage) {
        this.fingerid = fingerid;
        this.rollno = rollno;
        this.fingerimage = fingerimage;

    }

    public FingerDatabaseModel(

            int rollno,
            int fingerid,
            String fingerimage,
            String fingerstring,
            String candidateImage,
            String startDateTime,
            String endDateTime,
            int startFlag,
            int endFlag,
            int duration,
            String first_name,
            String last_name,
            String fathe_name,
            byte [] studentNewImage,
            byte [] studentOldImage
            ) {
        this.fingerid = fingerid;
        this.rollno = rollno;
        this.fingerimage = fingerimage;
        this.fingerstring = fingerstring;
        this.candidateImage = candidateImage;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.startFlag = startFlag;
        this.endFlag = endFlag;
        this.duration = duration;
        this.first_name = first_name;
        this.last_name = last_name;
        this.fathe_name = fathe_name;
        this.studentNewImage = studentNewImage;
        this.studentOldImage = studentOldImage;
    }

    public static final String CREATE_TABLE_FINGER_DB =
            "CREATE TABLE " + FINGER_DB_TABLE_NAME + "("
                    + COLUMN_ROllNO_FINGER_DB + " INTEGER,"
                    + COLUMN_FINGERID_FINGER_DB
                    + " INTEGER,"
                    + COLUMN_FINGERIMAGE_FINGER_DB + " TEXT,"
                    + COLUMN_FINGERSTRING_FINGER_DB + " TEXT,"
                    + COLUMN_CANDIDATEIMAGE_FINGER_DB + " TEXT,"
                    + COLUMN_FISTNAME_FINGER_DB + " TEXT,"
                    + COLUMN_LASTNAME_FINGER_DB + " TEXT,"
                    + COLUMN_FATHERNAE_FINGER_DB + " TEXT,"
                    + COLUMN_START_DATE_TIME_FINGER_DB + " TEXT,"
                    + COLUMN_END_DATE_TIME_FINGER_DB + " TEXT,"
                    + COLUMN_START_FLAG_FINGER_DB + " INTEGER,"
                    + COLUMN_END_FLAG_FINGER_DB + " INTEGER,"
                    + COLUMN_DURATION_FINGER_DB + " INTEGER,"
                    + COLUMN_CANDIDATEIMAGE_FINGER_DB_NEW + " BLOB,"
                    + COLUMN_CANDIDATEIMAGE_FINGER_DB_OLD + " BLOB"
                    + ")";

    //getter methods

    public int getRollno() {
        return rollno;
    }

    public int getFingerid() {
        return fingerid;
    }

    public String getCandidateImage() {
        return candidateImage;
    }

    public String getFingerimage() {
        return fingerimage;
    }

    public String getFingerstring() {
        return fingerstring;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public int getStartFlag() {
        return startFlag;
    }

    public int getEndFlag() {
        return endFlag;
    }

    public int getDuration() {
        return duration;
    }

    public String getColumnFistnameFingerDb() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getFathe_name() {
        return fathe_name;
    }

    public byte[] getStudentOldImage() {
        return studentOldImage;
    }

    public byte[] getStudentNewImage() {
        return studentNewImage;
    }
    //setter methods

    public void setCandidateImage(String candidateImage) {
        this.candidateImage = candidateImage;
    }

    public void setFingerid(int fingerid) {
        this.fingerid = fingerid;
    }

    public void setFingerimage(String fingerimage) {
        this.fingerimage = fingerimage;
    }

    public void setFingerstring(String fingerstring) {
        this.fingerstring = fingerstring;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }


    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }


    public void setStartFlag(int startFlag) {
        this.startFlag = startFlag;
    }

    public void setEndFlag(int endFlag) {
        this.endFlag = endFlag;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
