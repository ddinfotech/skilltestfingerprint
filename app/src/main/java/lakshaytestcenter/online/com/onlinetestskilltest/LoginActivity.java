package lakshaytestcenter.online.com.onlinetestskilltest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.Connectivity;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.ServiceApi;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;

public class LoginActivity extends AppCompatActivity {

    private Button mbtnSignIn,btnSignInOffline;
    private EditText metEmail, metPassword;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;
    SharedPreferences mPref;
    public static final String MYPREF = "user_info";
    SessionManager sessionManager;
    ProgressDialog progressDialog = null;
    ServiceApi serviceApi;
    ProgressDialog pDialog;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    FingerDatabaseHelper db;
    ArrayList<UserDetailObject> userList = new ArrayList<>();
    boolean  isUserData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //makeDirectory();
        sessionManager = new SessionManager(LoginActivity.this);
        db = new FingerDatabaseHelper(this);
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        if (sessionManager.isLoggedIn()) {
            if (sessionManager.ismportedData()) {
                Intent i = new Intent(LoginActivity.this, ValidateRollNumberActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        }
//
        serviceApi = new ServiceApi(LoginActivity.this);

        mbtnSignIn = (Button) findViewById(R.id.btnSignIn);
        metEmail = (EditText) findViewById(R.id.etEmail);
        metPassword = (EditText) findViewById(R.id.etPassword);
        btnSignInOffline = findViewById(R.id.btnSignInOffline);

        inputLayoutEmail = (TextInputLayout) findViewById(R.id.til_asignin_Email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.til_asignin_pass);



        metEmail.addTextChangedListener(new MyTextWatcher(metEmail));
        metPassword.addTextChangedListener(new MyTextWatcher(metPassword));


        btnSignInOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkValidationSignIn()) {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        checkPermissions();
                        Toast.makeText(LoginActivity.this, "Please allow Read write directiory permission...", Toast.LENGTH_SHORT).show();
                    }else {
                        pDialog.show();
                        final String importLoginData = readDataFormFile( "/" +"ShikshaApp" + "/LoginDetail.json");
                        if (importLoginData == null){
                            Toast.makeText(LoginActivity.this, "Login Data not found", Toast.LENGTH_SHORT).show();
                            pDialog.dismiss();
                        }else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    JSONArray jsonArray = null;
                                    try {
                                        jsonArray = new JSONArray(importLoginData);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            String username = jsonObject.getString("userid");
                                            String password = jsonObject.getString("password");
                                            UserDetailObject userDetailObject = new UserDetailObject(username,password);
                                            userList.add(userDetailObject);
                                        }
                                        isUserData = db.insertUserDetail(userList);
                                        if (isUserData){
                                            String password = db.getPassword(metEmail.getText().toString());
                                            String userPass = metPassword.getText().toString();
                                            if (password.equals(userPass)){
                                                sessionManager.createLoginSession(metEmail.getText().toString(), metPassword.getText().toString());
                                                if (sessionManager.ismportedData()) {
                                                    Intent i = new Intent(LoginActivity.this, ValidateRollNumberActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(i);
                                                    finish();
                                                }else {
                                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                    finish();
                                                }
                                            }else {
                                                Toast.makeText(LoginActivity.this, "email/password mismatch", Toast.LENGTH_SHORT).show();
                                            }
                                            pDialog.dismiss();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(LoginActivity.this, "Some error", Toast.LENGTH_SHORT).show();
                                        pDialog.dismiss();
                                    }
                                }
                            });

                        }
                    }
                }
            }
        });

        mbtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
//                     /*Start Method Call For Normal Login*/
                    if (Connectivity.isConnected(LoginActivity.this)) {
                        try {
                            pDialog.show();
                            new Login().execute(metEmail.getText().toString().trim(), metPassword.getText().toString().trim());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.wtf("response err", e.toString());
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "You have no internet connection...want to login offline mode", Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(LoginActivity.this, No_internet_found.class));
                    }
                     /*End Method Call For Normal Login*/
                }
            }
        });

    }

    private String  readDataFormFile(String path) {
        String result = null;
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), path);
            FileInputStream stream = new FileInputStream(yourFile);
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                result = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    private void makeDirectory(){
        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/" +"ShikshaApp");
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    class Login extends AsyncTask<String ,String,String>{

        ServiceApi serviceApi = new ServiceApi(LoginActivity.this);
        String result = "";
        @Override
        protected String doInBackground(String... args) {
            try {
               result =  serviceApi.adminLogin(args[0],args[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.wtf("response 111", result);
                if (result!=null && !result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    String Status = jsonObject.getString("Status");
                    if (Status.equals("1")) {
                        sessionManager.createLoginSession(metEmail.getText().toString(), metPassword.getText().toString());
                        if (sessionManager.ismportedData()) {
                            Intent i = new Intent(LoginActivity.this, ValidateRollNumberActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }else
                        {
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }

                    }else {
                        Toast.makeText(LoginActivity.this, "Password mismatch", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                pDialog.dismiss();
                ex.printStackTrace();
            }
        }
    }

    private boolean checkValidationSignIn() {

        if (!validateEmail()) {
            Log.d("msg", "not valid email");
            return false;
        }

        if (!validatePassword()) {
            Log.d("msg", "not valid pass");
            return false;
        }
        return true;
    }


    private boolean validateEmail() {
        String email = metEmail.getText().toString().trim();
        if (email.isEmpty()) {
            inputLayoutEmail.setError("Enter valid email");
            requestFocus(metEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }


    private boolean validatePassword() {
        if (metPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(metPassword);
            return false;
        } else {
            inputLayoutPassword.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etEmail:
                    validateEmail();
                    break;
                case R.id.etPassword:
                    validatePassword();
                    break;
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main, menu); //your file name
//        return super.onCreateOptionsMenu(menu);
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(final MenuItem item) {
//        int itemId = item.getItemId();
//        switch (itemId) {
//            case R.id.Export: {
//            }
//            break;
//            case R.id.FinalIn: {
//            }
//            break;
//            case R.id.FinalExit: {
//            }
//            break;
//            case R.id.ShortExitIn: {
//            }
//            break;
//            case R.id.ShortExitOut: {
//            }
//            break;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);

        }
    }
}
