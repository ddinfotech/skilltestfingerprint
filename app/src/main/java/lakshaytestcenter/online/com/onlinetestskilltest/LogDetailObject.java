package lakshaytestcenter.online.com.onlinetestskilltest;

public class LogDetailObject {
    int roll_number;
    String date;
    String time;
    String activity;
    int status;

    public LogDetailObject(int roll_number, String date, String activity, int status) {
        this.roll_number = roll_number;
        this.date = date;
        this.activity = activity;
        this.status = status;
    }

    public int getRoll_number() {
        return roll_number;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getActivity() {
        return activity;
    }

    public int getStatus() {
        return status;
    }
}
