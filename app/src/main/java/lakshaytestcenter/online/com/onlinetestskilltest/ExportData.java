package lakshaytestcenter.online.com.onlinetestskilltest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.model.CandidateAttendanceDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.Connectivity;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.ServiceApi;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.UtilManager;

import static android.os.Environment.getExternalStorageDirectory;
import static lakshaytestcenter.online.com.onlinetestskilltest.MainActivity.MY_PERMISSIONS_REQUEST_ACCESS_CODE;

public class ExportData extends AppCompatActivity {

    Button btnExport,btnExportOffline,cleanDatabase;
    AlertDialog alertDialog , dialog , closeDialog;
    FingerDatabaseHelper databaseHelper;
    ArrayList<LogDetailObject> logList = new ArrayList<>();
    ArrayList<ImageDetail> imageList = new ArrayList<>();
    SessionManager sessionManager;
    final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String CAPTURE_IMAGE_FILE_PROVIDER = "lakshaytestcenter.online.com.onlinetest.fileprovider";
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_data);
        btnExportOffline = findViewById(R.id.btnExportOffline);
        btnExport = findViewById(R.id.btnExport);
        cleanDatabase = findViewById(R.id.cleanDatabase);

        databaseHelper = new FingerDatabaseHelper(ExportData.this);
        pDialog = new ProgressDialog(ExportData.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        sessionManager=new SessionManager(ExportData.this);


        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to Export Online your log file");
            }
        });
        
        btnExportOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to Export your log file");
            }
        });
        cleanDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to clear your all database");
            }
        });
    }

    private void createDialog(String title , final String message){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_for_confirmation,null);
        alertBuilder.setView(view);
        dialog=alertBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        ImageView ivAlertCancel2 = view.findViewById(R.id.ivAlertCancel);
        TextView tvAlertMessage = view.findViewById(R.id.tvAlertMessage);
        TextView tvAlertTitle = view.findViewById(R.id.tvAlertTitle);
        Button okButton = view.findViewById(R.id.btnAlertOk);
        Button cancelButton = view.findViewById(R.id.btnAlertCancel);
        okButton.setText("ok");
        tvAlertTitle.setText(title);
        tvAlertMessage.setText(message);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.equalsIgnoreCase("Are you sure to clear your all database")) {
//                    writeToFile("","ActivityLog");
//                    writeToFile("","ImageList");
                    databaseHelper.clearAllTables();
                    sessionManager.logoutUser();
                    Intent intent = new Intent(ExportData.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    dialog.dismiss();
                }else if (message.equalsIgnoreCase("Are you sure to Export your log file")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String logResult = exportLogFileFromDatabase();
                            String imageResult = exportImageFromDatabase();
                            String attendanceResult = exportAttendanceDetailFromDatabase();
                            if (logResult !=null && imageResult !=null && attendanceResult!=null){
                                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                                    checkPermissions();
                                }else {
                                    writeToFile(logResult,"ActivityLog");
                                    writeToFile(imageResult,"ImageList");
                                    writeToFile(attendanceResult,"AttendanceList");
                                    Toast.makeText(ExportData.this, "Data Exported Successfully...", Toast.LENGTH_SHORT).show();
                                }
                                cleanDatabase.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(ExportData.this, "No Data found", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });

                }else if (message.equalsIgnoreCase("Are you sure to Export Online your log file")){
                    if (Connectivity.isConnected(ExportData.this)) {
                        try {
                            pDialog.show();
                            String data = exportLogFileFromDatabase();
                            if (data!=null) {
                                new ExportLogFile().execute(data);
                            }

                            String attendanceData = exportAttendanceDetailFromDatabase();
                            if (attendanceData!=null) {
                                new ExportAttendanceFile().execute(attendanceData);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            pDialog.show();
//                            Log.wtf("response err", e.toString());
                        }
                        dialog.dismiss();
                    } else {
//                        startActivity(new Intent(ExportData.this, No_internet_found.class));
                        Toast.makeText(ExportData.this, "You have no internet connection...", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        pDialog.dismiss();
                    }
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ivAlertCancel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public  void writeToFile(String body,String fileName)
    {
        FileOutputStream fos = null;
        try {
            final File dir = new File(getExternalStorageDirectory().getAbsolutePath() + "/ShikshaApp/" );
//            final File dir = new File(getFilesDir()  + "/ShikshaApp/" );
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            dir.setExecutable(true);
            dir.setReadable(true);
            dir.setWritable(true);
            MediaScannerConnection.scanFile(this, new String[] {dir.toString()}, null, null);
            final File myFile = new File(dir, fileName + ".json");
//            final File myFile = new File(getExternalStorageDirectory().getAbsolutePath() + "/ShikshaApp/", "ActivityLog" + ".txt");
            if (!body.equalsIgnoreCase("")) {
                if (myFile.exists())
                {
                    myFile.delete();
                    myFile.createNewFile();
                    myFile.setReadable(true);
                    myFile.setWritable(true);
                }else {
                    myFile.createNewFile();
                    myFile.setReadable(true);
                    myFile.setWritable(true);
                }
                fos = new FileOutputStream(myFile);
                fos.write(body.getBytes());
                fos.close();
            } else {
                if (myFile.exists())
                {
                    myFile.delete();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }

    public void writeFileOnInternalStorage(String sBody){
        FileOutputStream fos = null;
        File myDir = new File(getFilesDir(),"MyData");
        myDir.mkdir();
        File file = new File(myDir.getAbsolutePath(), "Log.txt");
        try {
            if (file.createNewFile()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            fos = new FileOutputStream(file);
            fos.write(sBody.getBytes());
            fos.close();

        }catch (Exception e){
            e.printStackTrace();

        }
    }


    public void sendFile(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String dirpath = context.getFilesDir() + File.separator + "directory";
        File file = new File(dirpath + File.separator + "file.txt");
        Uri uri = FileProvider.getUriForFile(context, CAPTURE_IMAGE_FILE_PROVIDER, file);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // Workaround for Android bug.
        // grantUriPermission also needed for KITKAT,
        // see https://code.google.com/p/android/issues/detail?id=76683
//        Uri imageUri = FileProvider.getUriForFile(ExportData.this, CAPTURE_IMAGE_FILE_PROVIDER, image);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    class ExportImage extends AsyncTask<String ,String,String> {

        ServiceApi serviceApi = new ServiceApi(ExportData.this);
        String result = "";
        @Override
        protected String doInBackground(String... args) {
            try {
                result =  serviceApi.exportImage(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("response 111", result);
                if (result!=null && !result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    String Status = jsonObject.getString("status");
                    if (Status.equals("1")) {
                        Toast.makeText(ExportData.this, "Image file Exported successfully...", Toast.LENGTH_SHORT).show();
                        cleanDatabase.setVisibility(View.VISIBLE);
                    }else {
                        Toast.makeText(ExportData.this, "Image file not Exported...please try again", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ExportData.this, "", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Toast.makeText(ExportData.this, "Some Exception...please try again", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }

    class ExportLogFile extends AsyncTask<String ,String,String> {

        ServiceApi serviceApi = new ServiceApi(ExportData.this);
        String result = "";
        @Override
        protected String doInBackground(String... args) {
            try {
                result =  serviceApi.exportLogFile(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("response 111", result);
                if (result!=null && !result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    String Status = jsonObject.getString("status");
                    if (Status.equals("1")) {
                        Toast.makeText(ExportData.this, "Log File Exported successfully...", Toast.LENGTH_SHORT).show();
//                        cleanDatabase.setVisibility(View.VISIBLE);
                        String imageResult = exportImageFromDatabase();
                        new ExportImage().execute(imageResult);
                    }else {
                        Toast.makeText(ExportData.this, "Log File not Exported...please try again", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ExportData.this, "", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Toast.makeText(ExportData.this, "Some Exception...please try again", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }

    private String exportLogFileFromDatabase() {
        String data ;
        logList = databaseHelper.getAllDataFromLogTable();
        JSONArray jsonArray = new JSONArray();
        if (logList.size() > 0) {
            boolean status;
            for (LogDetailObject log : logList) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("AdminUserName", sessionManager.getAdminUsername("name"));
                    jsonObject.put("RollNo", log.getRoll_number());
                    jsonObject.put("Date", log.getDate());
                    jsonObject.put("Activity", log.getActivity());
                    if (log.getStatus()==0){
                        status = false;
                    }else {
                        status = true;
                    }
                    jsonObject.put("Status", status);
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            data = jsonArray.toString();
        }else {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("AdminUserName", sessionManager.getAdminUsername("name"));
                jsonObject.put("RollNo", "");
                jsonObject.put("Date", "");
                jsonObject.put("Activity", "No Activity found");
                jsonObject.put("Status", false);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data = jsonArray.toString();
        }
        return data;
    }

    private String exportImageFromDatabase() {
        String data ;
        imageList = databaseHelper.getAllImage();
        JSONArray jsonArray = new JSONArray();
        int rollNumber = 0;
        if (imageList.size() > 0) {
            for (ImageDetail log : imageList) {
                String firstImage = "" , secondImage = "";
                try {
                    JSONObject jsonObject = new JSONObject();
                    if (log.getOldImage() != null) {
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(log.getOldImage());
                        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                        firstImage = convertImageToBaseString(theImage);
                        rollNumber = log.getRollNumber();
                    }
                    if (log.getNewImage() != null){
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(log.getNewImage());
                        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                        secondImage = convertImageToBaseString(theImage);
                    }
                        jsonObject.put("RollNo", log.getRollNumber());
                        jsonObject.put("NewImage", firstImage);
                        jsonObject.put("OldImage", secondImage);
                        jsonArray.put(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            data = jsonArray.toString();
        } else {
            try {
                String firstImage = "" , secondImage = "";
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("RollNo", "");
                jsonObject.put("NewImage", firstImage);
                jsonObject.put("OldImage", secondImage);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data = jsonArray.toString();
        }
        return data;
    }

    public String convertImageToBaseString(Bitmap bitmap){
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte [] ba = bao.toByteArray();
        String ba1= Base64.encodeToString(ba, Base64.NO_WRAP);
        return ba1;
    }

    private String exportAttendanceDetailFromDatabase() {
        String data ;
        ArrayList<CandidateAttendanceDetailObject> attendanceList = databaseHelper.getAllAttendanceList();
        JSONArray jsonArray = new JSONArray();
        int rollNumber = 0;
        if (attendanceList.size() > 0) {
            for (CandidateAttendanceDetailObject log : attendanceList) {
                try {
                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("RollNo", log.getRollNo());
//                    jsonObject.put("FingerString", log.getCapturedFingerString());
//                    jsonObject.put("FingerImage", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedFingerImage())));
//                    jsonObject.put("ProfileImage", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedProfileImage())));
//                    jsonObject.put("Signature", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedSignature())));
//                    jsonObject.put("FP_matchingStatus", log.getFingerPrintMatchingStatus());
//                    jsonObject.put("AttendanceStatus", log.getAttendanceStatus());

                    jsonObject.put("in_roll_no", log.getRollNo());
                    jsonObject.put("nvc_finger_string", log.getCapturedFingerString());
                    jsonObject.put("nvc_finger_image", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedFingerImage())));
                    jsonObject.put("nvc_profile_image", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedProfileImage())));
                    jsonObject.put("nvc_signature", convertImageToBaseString(UtilManager.loadImageFromStorage(log.getCapturedSignature())));
                    jsonObject.put("nvc_fp_matchingStatus", log.getFingerPrintMatchingStatus());
                    jsonObject.put("nvc_attendance_status", log.getAttendanceStatus());
                    jsonArray.put(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            data = jsonArray.toString();
        } else {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("in_roll_no", "");
                jsonObject.put("nvc_finger_string", "");
                jsonObject.put("nvc_finger_image", "");
                jsonObject.put("nvc_profile_image", "");
                jsonObject.put("nvc_signature", "");
                jsonObject.put("nvc_fp_matchingStatus", "");
                jsonObject.put("nvc_attendance_status", "");
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data = jsonArray.toString();
        }
        return data;
    }

    class ExportAttendanceFile extends AsyncTask<String ,String,String> {

        ServiceApi serviceApi = new ServiceApi(ExportData.this);
        String result = "";
        @Override
        protected String doInBackground(String... args) {
            try {
                result =  serviceApi.exportAttendance(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("response 111", result);
                if (result!=null && !result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    String Status = jsonObject.getString("status");
                    if (Status.equals("1")) {
                        Toast.makeText(ExportData.this, "Attendance File Exported successfully...", Toast.LENGTH_SHORT).show();
//                        cleanDatabase.setVisibility(View.VISIBLE);
                        String imageResult = exportImageFromDatabase();
                        new ExportImage().execute(imageResult);
                    }else {
                        Toast.makeText(ExportData.this, "Attendance File not Exported...please try again", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ExportData.this, "", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Toast.makeText(ExportData.this, "Some Exception...please try again", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }
}
