package lakshaytestcenter.online.com.onlinetestskilltest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;

import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseModel;
import lakshaytestcenter.online.com.onlinetestskilltest.attendance.CandidateAttendenceActivity;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.Connectivity;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.ServiceApi;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;

import static android.os.Environment.getExternalStorageDirectory;
import static lakshaytestcenter.online.com.onlinetestskilltest.MainActivity.MY_PERMISSIONS_REQUEST_ACCESS_CODE;

public class ValidateRollNumberActivity extends AppCompatActivity {
    private Button btnValidate,btn_finalExit,btn_temp_out,btn_tempIn,exportData,cleanDatabase,exportlogOnline,exportedData;
    private EditText et_roll_num;
    private TextInputLayout til_assign_roll_num;
    SharedPreferences mPref;
    public static final String MYPREF = "user_info";
    FingerDatabaseHelper databaseHelper;
    SessionManager sessionManager;
    AlertDialog alertDialog , dialog , closeDialog;
    ArrayList<LogDetailObject> logList = new ArrayList<>();
    ArrayList<ImageDetail> imageList = new ArrayList<>();
    final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String CAPTURE_IMAGE_FILE_PROVIDER = "lakshaytestcenter.online.com.onlinetest.fileprovider";
    ProgressDialog pDialog;
    Button btnTakeAttendance;
    ImageView ivQRcode;
    TextView tvTotalCandidateCount, tvMappedCandidateCount, tvPresentCandidateCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_roll_number);
        databaseHelper = new FingerDatabaseHelper(ValidateRollNumberActivity.this);
        pDialog = new ProgressDialog(ValidateRollNumberActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        tvTotalCandidateCount = (TextView) findViewById(R.id.tvTotalCandidateCount);
        tvMappedCandidateCount = (TextView) findViewById(R.id.tvMappedCandidateCount);
        tvPresentCandidateCount = (TextView) findViewById(R.id.tvPresentCandidateCount);

        btnValidate = (Button) findViewById(R.id.btn_finalIn);
        ivQRcode = (ImageView) findViewById(R.id.ivQRcode);
        btn_finalExit = (Button) findViewById(R.id.btn_finalExit);
        btn_temp_out = (Button) findViewById(R.id.btn_temp_out);
        btn_tempIn = (Button) findViewById(R.id.btn_tempIn);
        et_roll_num = (EditText) findViewById(R.id.et_roll_num);
        til_assign_roll_num = (TextInputLayout) findViewById(R.id.til_assign_roll_num);
        et_roll_num.addTextChangedListener(new MyTextWatcher(et_roll_num));
        sessionManager=new SessionManager(ValidateRollNumberActivity.this);
        exportData = findViewById(R.id.exportData);
        cleanDatabase = findViewById(R.id.cleanDatabase);
        exportedData = findViewById(R.id.exportedData);
        exportlogOnline = findViewById(R.id.exportDataOnline);
        btnTakeAttendance = findViewById(R.id.btnTakeAttendance);
        //sendFile(this);
        /**** cheking fingue print first time ********************/
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
                    FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(et_roll_num.getText().toString().trim());
                    if (databaseModel.getRollno() != -1) {
                        Intent i = new Intent(ValidateRollNumberActivity.this, StudentDetailsActiviy.class);
                        String roll= String.valueOf(databaseModel.getRollno());
                        i.putExtra("roll", roll);
                        i.putExtra("final", "finalIn");
                        startActivity(i);
                        Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
                    } else {
                        Toast.makeText(ValidateRollNumberActivity.this, " Sorry No Match Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        exportData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to Export your log file");
            }
        });

        exportedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ValidateRollNumberActivity.this,ExportData.class);
                startActivity(intent);
            }
        });

        exportlogOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to Export Online your log file");

            }
        });


/***************final exit*******************/
        btn_finalExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
                    FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(et_roll_num.getText().toString().trim());
                    if (databaseModel.getRollno() != -1) {
                        Intent i = new Intent(ValidateRollNumberActivity.this, StudentDetailsActiviy.class);
                        String roll= String.valueOf(databaseModel.getRollno());
                        i.putExtra("roll", roll);
                        i.putExtra("final", "final exit");
                        startActivity(i);
                        Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
                    } else {
                        Toast.makeText(ValidateRollNumberActivity.this, " Sorry No Match Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

/************************temp out**********************/
        btn_temp_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
                    FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(et_roll_num.getText().toString().trim());
                    if (databaseModel.getRollno() != -1) {
                        Intent i = new Intent(ValidateRollNumberActivity.this, StudentDetailsActiviy.class);
                        String roll= String.valueOf(databaseModel.getRollno());
                        i.putExtra("roll", roll);
                        i.putExtra("final", "temp out");
                        startActivity(i);
                        Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
                    } else {
                        Toast.makeText(ValidateRollNumberActivity.this, " Sorry No Match Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        /*************************tempIn**************************/
        btn_tempIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
                    FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(et_roll_num.getText().toString().trim());
                    if (databaseModel.getRollno() != -1) {
                        Intent i = new Intent(ValidateRollNumberActivity.this, StudentDetailsActiviy.class);
                        String roll= String.valueOf(databaseModel.getRollno());
                        i.putExtra("roll", roll);
                        i.putExtra("final", "temp in");
                        startActivity(i);
                        Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
                    } else {
                        Toast.makeText(ValidateRollNumberActivity.this, " Sorry No Match Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnTakeAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationSignIn()) {
                    FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(et_roll_num.getText().toString().trim());
                    if (databaseModel.getRollno() != -1) {
                        Intent i = new Intent(ValidateRollNumberActivity.this, CandidateAttendenceActivity.class);
                        String roll= String.valueOf(databaseModel.getRollno());
                        i.putExtra("roll", roll);
                        i.putExtra("final", "temp in");
                        startActivity(i);
                        Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
                    } else {
                        Toast.makeText(ValidateRollNumberActivity.this, " Sorry No Match Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ivQRcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new IntentIntegrator(ValidateRollNumberActivity.this).initiateScan();

//                IntentIntegrator integrator = new IntentIntegrator(ValidateRollNumberActivity.this);
//                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
//                integrator.setPrompt("Scan a barcode");
//                integrator.setOrientationLocked(false);
//                integrator.setCaptureActivity(CustomScannerActivity.class);
////                integrator.setCameraId(0);  // Use a specific camera of the device
//                integrator.setBeepEnabled(false);
//                integrator.setBarcodeImageEnabled(true);
//                integrator.initiateScan();

                new IntentIntegrator(ValidateRollNumberActivity.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan();

            }
        });

        cleanDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Alert !!","Are you sure to clear your all database");
            }
        });

        updateAttendanceCount();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                et_roll_num.setText(result.getContents());
//                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean checkValidationSignIn() {

        if (!validateEmail()) {
            Log.d("msg", "not valid email");
            return false;
        }

        return true;
    }


    private boolean validateEmail() {
        String email = et_roll_num.getText().toString().trim();
        if (email.isEmpty()) {
            til_assign_roll_num.setError(getString(R.string.err_msg_email));
            requestFocus(et_roll_num);
            return false;
        } else {
            til_assign_roll_num.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_roll_num:
                    validateEmail();
                    break;

            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main, menu); //your file name
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(final MenuItem item) {
//        int itemId = item.getItemId();
//        switch (itemId) {
//            case R.id.action_out:
//            case R.id.action_clearDatabase:
//                createDialog("Alert !!","Are you sure to clear your all database");
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    private void createDialog(String title , final String message){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_for_confirmation,null);
        alertBuilder.setView(view);
        dialog=alertBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        ImageView ivAlertCancel2 = view.findViewById(R.id.ivAlertCancel);
        TextView tvAlertMessage = view.findViewById(R.id.tvAlertMessage);
        TextView tvAlertTitle = view.findViewById(R.id.tvAlertTitle);
        Button okButton = view.findViewById(R.id.btnAlertOk);
        Button cancelButton = view.findViewById(R.id.btnAlertCancel);
        okButton.setText("ok");
        tvAlertTitle.setText(title);
        tvAlertMessage.setText(message);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.equalsIgnoreCase("Are you sure to clear your all database")) {
                    databaseHelper.clearAllTables();
                    sessionManager.logoutUser();
                    Intent intent = new Intent(ValidateRollNumberActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    dialog.dismiss();
                }else if (message.equalsIgnoreCase("Are you sure to Export your log file")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String logResult = exportLogFileFromDatabase();
                            String imageResult = exportImageFromDatabase();
                            if (logResult !=null && imageResult !=null ){
                                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                                    checkPermissions();
                                }else {
                                    writeToFile(logResult,"ActivityLog");
                                    writeToFile(imageResult,"ImageList");
                                    Toast.makeText(ValidateRollNumberActivity.this, "Data Exported Successfully...", Toast.LENGTH_SHORT).show();
                                }
                                cleanDatabase.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(ValidateRollNumberActivity.this, "No Data found", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });

                }else if (message.equalsIgnoreCase("Are you sure to Export Online your log file")){
                    if (Connectivity.isConnected(ValidateRollNumberActivity.this)) {
                        try {
                            pDialog.show();
                            String data = exportLogFileFromDatabase();
                            if (data!=null) {
                                new ExportLogFile().execute(data);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            pDialog.show();
//                            Log.wtf("response err", e.toString());
                        }
                        dialog.dismiss();
                    } else {
//                        startActivity(new Intent(ValidateRollNumberActivity.this, No_internet_found.class));
                        Toast.makeText(ValidateRollNumberActivity.this, "You have no internet connection...", Toast.LENGTH_SHORT).show();
                        pDialog.show();
                        dialog.dismiss();
                    }
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ivAlertCancel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


//    public  void writeToFile3(String body)
//    {
//        FileOutputStream fos = null;
////        File file = new File( Environment.getExternalStorageDirectory(), "picture.txt" );
//        File file;
//        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
//            file = new File(Environment.getExternalStoragePublicDirectory(
//                    Environment.DIRECTORY_DOWNLOADS), "Log.txt");
//        } else {
//            file = new File(getFilesDir(), "Log.txt");
//        }
//        try {
//            fos = new FileOutputStream(file.getPath());
//            fos.write(body.getBytes());
//            fos.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public  void writeToFile2(String body)
//    {
//        FileOutputStream fos = null;
//        try {
////            File dcimDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
//            File picsDir = new File(getExternalFilesDir(null), "/ShikshaApp/");
//            picsDir.mkdirs(); //make if not exist
//            File myFile = new File(picsDir, "ActivityLog.txt");
//            if (!myFile.exists())
//            {
//                myFile.createNewFile();
//                myFile.setReadable(true);
//                myFile.setWritable(true);
//            }
//            fos = new FileOutputStream(myFile);
////            fos = openFileOutput("ActivityLog.txt",MODE_PRIVATE);
//            fos.write(body.getBytes());
//            fos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


    public  void writeToFile(String body,String fileName)
    {
        FileOutputStream fos = null;
        try {
            final File dir = new File(getExternalStorageDirectory().getAbsolutePath() + "/ShikshaApp/" );
//            final File dir = new File(getFilesDir()  + "/ShikshaApp/" );
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            dir.setExecutable(true);
            dir.setReadable(true);
            dir.setWritable(true);
            MediaScannerConnection.scanFile(this, new String[] {dir.toString()}, null, null);
            final File myFile = new File(dir, fileName + ".json");
//            final File myFile = new File(getExternalStorageDirectory().getAbsolutePath() + "/ShikshaApp/", "ActivityLog" + ".txt");
            if (!myFile.exists())
            {
                myFile.createNewFile();
                myFile.setReadable(true);
                myFile.setWritable(true);
            }
            fos = new FileOutputStream(myFile);
            fos.write(body.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);
        }
    }

    public void writeFileOnInternalStorage(String sBody){
        FileOutputStream fos = null;
        File myDir = new File(getFilesDir(),"MyData");
        myDir.mkdir();
       File file = new File(myDir.getAbsolutePath(), "Log.txt");
        try {
            if (file.createNewFile()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            fos = new FileOutputStream(file);
            fos.write(sBody.getBytes());
            fos.close();

        }catch (Exception e){
            e.printStackTrace();

        }
    }


    public void sendFile(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String dirpath = context.getFilesDir() + File.separator + "directory";
        File file = new File(dirpath + File.separator + "file.txt");
        Uri uri = FileProvider.getUriForFile(context, CAPTURE_IMAGE_FILE_PROVIDER, file);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // Workaround for Android bug.
        // grantUriPermission also needed for KITKAT,
        // see https://code.google.com/p/android/issues/detail?id=76683
//        Uri imageUri = FileProvider.getUriForFile(ValidateRollNumberActivity.this, CAPTURE_IMAGE_FILE_PROVIDER, image);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    class ExportLogFile extends AsyncTask<String ,String,String> {

        ServiceApi serviceApi = new ServiceApi(ValidateRollNumberActivity.this);
        String result = "";
        @Override
        protected String doInBackground(String... args) {
            try {
                result =  serviceApi.exportLogFile(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("response 111", result);
                if (result!=null && !result.equals("")) {
                    JSONObject jsonObject = new JSONObject(result);
                    String Status = jsonObject.getString("status");
                    if (Status.equals("1")) {
                        Toast.makeText(ValidateRollNumberActivity.this, "Data Exported successfully...", Toast.LENGTH_SHORT).show();
                        cleanDatabase.setVisibility(View.VISIBLE);
                    }else {
                        Toast.makeText(ValidateRollNumberActivity.this, "Data not Exported...please try again", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ValidateRollNumberActivity.this, "", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                ex.printStackTrace();
                Toast.makeText(ValidateRollNumberActivity.this, "Some Exception...please try again", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }

    private String exportLogFileFromDatabase() {
        String data ;
        logList = databaseHelper.getAllDataFromLogTable();
        if (logList.size() > 0) {
            JSONArray jsonArray = new JSONArray();
            for (LogDetailObject log : logList) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("RollNo", log.getRoll_number());
                    jsonObject.put("Date", log.getDate());
                    jsonObject.put("Activity", log.getActivity());
                    jsonObject.put("Status", String.valueOf(log.getStatus()));
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            data = jsonArray.toString();
        }else {
            data = null;
        }
        return data;
    }

    private String exportImageFromDatabase() {
        String data ;
        imageList = databaseHelper.getAllImage();
        if (imageList.size() > 0) {
            JSONArray jsonArray = new JSONArray();
            for (ImageDetail log : imageList) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("RollNo", log.getRollNumber());
//                    jsonObject.put("CandidateFirstImage", log.getOldImage());
//                    jsonObject.put("CandidateNewImage", log.getNewImage());
                    String firstImage = null , secondImage = null;
                    if (log.getOldImage() != null) {
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(log.getOldImage());
                        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                        firstImage = convertImageToBaseString(theImage);
                    }
                    if (log.getNewImage() != null){
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(log.getNewImage());
                        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                        secondImage = convertImageToBaseString(theImage);
                    }
                    if (log.getOldImage() != null || log.getNewImage() != null) {
                        jsonObject.put("CandidateFirstImage", firstImage);
                        jsonObject.put("CandidateNewImage", secondImage);
                        jsonArray.put(jsonObject);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            data = jsonArray.toString();
        }else {
            data = null;
        }
        return data;
    }

    public String convertImageToBaseString(Bitmap bitmap){
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte [] ba = bao.toByteArray();
        String ba1= Base64.encodeToString(ba, Base64.DEFAULT);
        return ba1;
    }

    public void updateAttendanceCount() {
//        TextView tvTotalCandidateCount, tvMappedCandidateCount, tvPresentCandidateCount;

        tvTotalCandidateCount.setText(databaseHelper.getTotalCandidateCount());
        tvMappedCandidateCount.setText(databaseHelper.getMappedCandidateCount());
        tvPresentCandidateCount.setText(databaseHelper.getPresentCandidateCount());

    }
}

