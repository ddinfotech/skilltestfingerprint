package lakshaytestcenter.online.com.onlinetestskilltest.utilities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import lakshaytestcenter.online.com.onlinetestskilltest.R;

public class No_internet_found extends AppCompatActivity {
    Button mbtn_tryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet_found);
        mbtn_tryAgain = (Button) findViewById(R.id.btn_tryAgain);
        mbtn_tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Connectivity.isConnected(No_internet_found.this)) {
                    finish();
                } else {
                    Intent i = new Intent(No_internet_found.this, No_internet_found.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {

         onBackPressed();
    }
}
