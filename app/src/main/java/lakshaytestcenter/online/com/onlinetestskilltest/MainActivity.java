package lakshaytestcenter.online.com.onlinetestskilltest;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseModel;
import lakshaytestcenter.online.com.onlinetestskilltest.model.CandidateDetailObject;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.Connectivity;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.ServiceApi;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    FingerDatabaseModel databaseModel;
    ServiceApi serviceApi;
    FingerDatabaseHelper databaseHelper;
    SessionManager sessionManager;
    Button button,btnImportOffline;
    ProgressDialog pDialog;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    final String[] requiredPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    boolean isCandidateDetail , isCandidateFinger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        databaseModel = new FingerDatabaseModel();
        sessionManager = new SessionManager(MainActivity.this);
        databaseHelper = new FingerDatabaseHelper(MainActivity.this);
        serviceApi = new ServiceApi(MainActivity.this);
        Button btnImport = (Button) findViewById(R.id.btnImport);
        btnImportOffline = findViewById(R.id.btnImportOffline);

        btnImport.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             if (Connectivity.isConnected(MainActivity.this)) {
                                                 try {
                                                     pDialog.show();
                                                     new ImportDataTask().execute();
                                                     new ImportCanditateDataTask().execute();
                                                 } catch (Exception e) {
                                                     e.printStackTrace();
                                                     Log.wtf("response err", e.toString());
                                                 }
                                             }
                                             else {
                                                 Toast.makeText(MainActivity.this, "You have no internet connection...want to download offline data", Toast.LENGTH_SHORT).show();
//                                                 startActivity(new Intent(MainActivity.this, No_internet_found.class));
                                             }
                                         }
                                     }
        );

        btnImportOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    checkPermissions();
                    Toast.makeText(MainActivity.this, "Please allow Read write directiory permission...", Toast.LENGTH_SHORT).show();
                } else {
                    pDialog.show();
                    final String importCandidateData = readDataFormFile( "/" +"ShikshaApp" + "/CandidateDetail.json");
//                    String importFingerDetails = readDataFormFile("/" +"ShikshaApp" + "/CandidateFingerDetail.txt");
                    final String importFingerDetails = readDataFormFile("/" +"ShikshaApp" + "/CandidateFingerDetail.json");
                    if (importCandidateData==null || importFingerDetails==null){
                        Toast.makeText(MainActivity.this, "File not found", Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                JSONArray jsonArray = null;
                                try {
                                    jsonArray = new JSONArray(importFingerDetails);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String fingerid = jsonObject.getString("fingerid");
                                        String fingerstring = jsonObject.optString("fingerstring");
                                        String rollno = jsonObject.getString("rollno");
                                        String fingerimage = jsonObject.getString("fingerimage");
                                        String candidateImage = jsonObject.optString("candidateImage");
                                        insertBiometricData(jsonObject);
                                        databaseHelper.insertData(Integer.parseInt(rollno), fingerid, fingerimage, fingerstring, candidateImage, "", "", 0, 0, 0);
                                        sessionManager.createImportData();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Log.wtf("response 113", importCandidateData);
                                    JSONArray jsonArray1 = new JSONArray(importCandidateData);
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject jsonObject = jsonArray1.getJSONObject(i);
                                        String first_name = jsonObject.getString("first_name");
                                        String rollno = jsonObject.getString("roll_no");
                                        String last_name = jsonObject.getString("last_name");
                                        String father_name = jsonObject.getString("father_name");
                                        insertCandidateData(jsonObject);
                                        databaseHelper.updateData(rollno, first_name, last_name, father_name);
                                        sessionManager.createImportData();
                                    }
                                    pDialog.dismiss();
                                    startActivity(new Intent(MainActivity.this, ValidateRollNumberActivity.class));
                                    finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.wtf("response err ff", e.toString());
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void insertBiometricData(JSONObject jsonObject) throws JSONException {
        CandidateDetailObject candidateDetailObject = new CandidateDetailObject();
        candidateDetailObject.setFingerId(jsonObject.getString("fingerid"));
        candidateDetailObject.setFingerString(jsonObject.getString("fingerstring"));
        candidateDetailObject.setRollNo(jsonObject.getString("rollno"));
        candidateDetailObject.setCandidateFingerImage(jsonObject.getString("fingerimage"));
        candidateDetailObject.setCandidateProfileImage(jsonObject.optString("candidateImage"));
        candidateDetailObject.setCandidateSignature(jsonObject.optString("signature"));
        candidateDetailObject.setAllottedLabSeat(jsonObject.optString("pc_user_name"));
        databaseHelper.insertBiometricDetail(candidateDetailObject);
    }

    private void insertCandidateData(JSONObject jsonObject) throws JSONException {
        CandidateDetailObject candidateDetailObject = new CandidateDetailObject();
        candidateDetailObject.setDetailId(jsonObject.optString("id_candidate_detail"));
        candidateDetailObject.setRollNo(jsonObject.getString("roll_no"));
        candidateDetailObject.setFirstName(jsonObject.getString("first_name"));
        candidateDetailObject.setLastName(jsonObject.getString("last_name"));
        candidateDetailObject.setFatherName(jsonObject.getString("father_name"));
        candidateDetailObject.setDateOfBirth(jsonObject.getString("date_of_birth"));
        candidateDetailObject.setAddress1(jsonObject.getString("address1"));
        candidateDetailObject.setAddress2(jsonObject.getString("address2"));
        candidateDetailObject.setState(jsonObject.getString("state"));
        candidateDetailObject.setCity(jsonObject.getString("city"));
        candidateDetailObject.setPincode(jsonObject.getString("pincode"));
        candidateDetailObject.setProfilePicture(jsonObject.getString("picture"));
        databaseHelper.insertCandidateDetail(candidateDetailObject);
    }

    private String  readDataFormFile(String path) {
        String result = null;
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), path);
            FileInputStream stream = new FileInputStream(yourFile);
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                result = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public class ImportDataTask extends AsyncTask<String, Void, String> {
        ServiceApi serviceApi = new ServiceApi(MainActivity.this);
        String result = "";

        @Override
        protected void onPostExecute(final String success) {
           // pDialog.dismiss();
            isCandidateFinger = true;
            if (result != null && !result.equals(""))
            {
                JSONArray jsonArray = null;
                try {
                    Log.d("Response finger",result);
                    jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String fingerid = jsonObject.getString("fingerid");
                        String fingerstring = jsonObject.getString("fingerstring");
                        String rollno = jsonObject.getString("rollno");
                        String fingerimage = jsonObject.getString("fingerimage");
                        String candidateImage = jsonObject.getString("candidateImage");
                        insertBiometricData(jsonObject);
                        databaseHelper.insertData(Integer.parseInt(rollno), fingerid, fingerimage, fingerstring, candidateImage, "", "", 0, 0, 0);
                        sessionManager.createImportData();
                    }
                    if (isCandidateDetail && isCandidateFinger) {
                        pDialog.dismiss();
                        startActivity(new Intent(MainActivity.this, ValidateRollNumberActivity.class));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(MainActivity.this, "No Data found ", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(success);
        }

        protected String doInBackground(final String... args) {
            try {
                result = serviceApi.ImportData();
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return null;
        }
    }


    public class ImportCanditateDataTask extends AsyncTask<String, Void, String> {
        ServiceApi serviceApi = new ServiceApi(MainActivity.this);
        String result = "";
        protected String doInBackground(final String... args) {
            String r = null;
            try {
                result = serviceApi.ImportCanditateData();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
            return r;
        }
        @Override
        protected void onPostExecute(final String success) {
            if (pDialog.isShowing()) {
//                pDialog.dismiss();
                isCandidateDetail = true;
                if (result != null && !result.equals("")) {
                    try {
                        Log.d("response 113", result);
                        JSONArray jsonArray1 = new JSONArray(result);
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject jsonObject = jsonArray1.getJSONObject(i);
                            String first_name = jsonObject.getString("first_name");
                            String rollno = jsonObject.getString("roll_no");
                            String last_name = jsonObject.getString("last_name");
                            String father_name = jsonObject.getString("father_name");
                            insertCandidateData(jsonObject);
                            databaseHelper.updateData(rollno, first_name, last_name, father_name);
                            sessionManager.createImportData();
                        }
                        if (isCandidateDetail && isCandidateFinger) {
                            pDialog.dismiss();
                            startActivity(new Intent(MainActivity.this, ValidateRollNumberActivity.class));
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.wtf("response err ff", e.toString());
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No Data found", Toast.LENGTH_SHORT).show();

                }
            }

            super.onPostExecute(success);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_CODE: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
            }
        }
    }

    /**
     * checking  permissions at Runtime.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        final List<String> neededPermissions = new ArrayList<>();
        for (final String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                neededPermissions.add(permission);
            }
        }
        if (!neededPermissions.isEmpty()) {
            requestPermissions(neededPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUEST_ACCESS_CODE);

        }
    }
}



