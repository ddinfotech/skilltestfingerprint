package lakshaytestcenter.online.com.onlinetestskilltest;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.mantra.mfs100.FingerData;
import com.mantra.mfs100.MFS100;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.FingerDatabaseModel;
import lakshaytestcenter.online.com.onlinetestskilltest.DataBase.TempIODatabaseHelper;
import lakshaytestcenter.online.com.onlinetestskilltest.utilities.SessionManager;

import com.mantra.mfs100.MFS100Event;

public class StudentDetailsActiviy extends AppCompatActivity implements MFS100Event {


    TextView txtEventLog;
    public static String candidateImage,fingerstring,fingureImage;
    CheckBox cbFastDetection;
    EditText fmFirstNameInput, fmLastNameInput, fmFatherNameInput;
    byte[] Enroll_Template;
    byte[] Verify_Template;
    public static FingerData lastCapFingerData = null,scanData;
    ScannerAction scannerAction = ScannerAction.Capture;
    int timeout = 10000;
    MFS100 mfs100 = null;
    private boolean isCaptureRunning = false;
    ImageView fmStudentImageView, fmscanPrintImageView, fmFingerPrintImageView,fmStudentImageViewNew,fmStudentImageViewOld;
    TextView fmRollnoInput;
    Button fmMatch, fmcapture,fmcaptureImage,fmSaveLog;
    public String finalText;
    SessionManager sessionManager;
    FingerDatabaseHelper databaseHelper;
    TempIODatabaseHelper tempIODatabaseHelper;
    String roll_no;
    FingerDatabaseHelper db;
    int fingerPrintCount = 0 ;


    private enum ScannerAction {
        Capture, Verify
    }
    Toolbar toolbar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.studentsdetails_activity_main);
        setToolbar();
        initClass();
        initView();
        initGetData();
    }

    private void initView(){
        fmStudentImageView = (ImageView) findViewById(R.id.fmStudentImageView);
        fmFingerPrintImageView = (ImageView) findViewById(R.id.fmFingerPrintImageView);
        fmscanPrintImageView = (ImageView) findViewById(R.id.fmscanPrintImageView);
        fmRollnoInput = (TextView) findViewById(R.id.fmRollnoInput);
        txtEventLog = (TextView) findViewById(R.id.txtEventLog);
        fmFirstNameInput = (EditText) findViewById(R.id.fmFirstNameInput);
        fmLastNameInput = (EditText) findViewById(R.id.fmLastNameInput);
        fmFatherNameInput = (EditText) findViewById(R.id.fmFatherNameInput);
        fmcaptureImage = findViewById(R.id.fmcaptureImage);
        fmStudentImageViewNew = findViewById(R.id.fmStudentImageViewNew);
        //fmMatch = (Button) findViewById(R.id.fmSavebtn);
        fmcapture = (Button) findViewById(R.id.fmcapture);
        fmSaveLog = findViewById(R.id.fmSaveLog);
        sessionManager = new SessionManager(StudentDetailsActiviy.this);
        databaseHelper = new FingerDatabaseHelper(StudentDetailsActiviy.this);
        fmStudentImageViewOld = findViewById(R.id.fmStudentImageViewOld);

        fmcapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                scannerAction = ScannerAction.Capture;
                if (!isCaptureRunning) {
                    StartSyncCapture();
                }
            }
        });

        fmcaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraIntent();
            }
        });

    }

    private void initClass(){
        db = new FingerDatabaseHelper(this);
        Bundle i = getIntent().getExtras();
        if (i != null) {
            roll_no = i.getString("roll");
            finalText = i.getString("final");

        } else {
            Toast.makeText(getApplicationContext(), "result not found", Toast.LENGTH_LONG).show();
        }

        setActionbarTitle(finalText);
    }

    private void setActionbarTitle(String finalText){
        String title = "";
        if (finalText.equalsIgnoreCase("finalIn")){
            title = "First In";
        } else if (finalText.equalsIgnoreCase("final exit")){
            title = "Last Out";
        } else if (finalText.equalsIgnoreCase("temp in")){
            title = "Temp In";
        } else if (finalText.equalsIgnoreCase("temp out")){
            title = "Temp Out";
        }
        getSupportActionBar().setTitle(title);
    }


    private void initGetData(){

        FingerDatabaseModel databaseModel=databaseHelper.getDatabaseModel(roll_no);
        if (databaseModel.getRollno() != -1) {
            String roll= String.valueOf(databaseModel.getRollno());
            fingureImage = String.valueOf(databaseModel.getFingerimage());
            String fingureId= String.valueOf(databaseModel.getFingerid());
            fingerstring = String.valueOf(databaseModel.getFingerstring());
            candidateImage = String.valueOf(databaseModel.getCandidateImage());
            String first_name= String.valueOf(databaseModel.getColumnFistnameFingerDb());
            String last_name= String.valueOf(databaseModel.getLast_name());
            String father_name= String.valueOf(databaseModel.getFathe_name());
            byte [] studentNewImage = databaseModel.getStudentNewImage();
            byte [] studentOldImage = databaseModel.getStudentOldImage();
            Log.wtf("response roll", String.valueOf(databaseModel.getFingerid()));
            fmStudentImageView.setImageBitmap(getBitmpa(candidateImage));
            fmFingerPrintImageView.setImageBitmap(getBitmpa(fingureImage));
            fmRollnoInput.setText("Roll No is:  " + roll_no);
            fmFirstNameInput.setText(first_name);
            fmLastNameInput.setText(last_name);
            fmFatherNameInput.setText(father_name);
            if (studentNewImage !=null) {
                ByteArrayInputStream imageStream = new ByteArrayInputStream(studentNewImage);
                Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                fmStudentImageViewNew.setImageBitmap(theImage);
            }
            if (studentOldImage !=null) {
                ByteArrayInputStream imageStream = new ByteArrayInputStream(studentOldImage);
                Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                fmStudentImageViewOld.setImageBitmap(theImage);
            }

        }
    }

    public Bitmap getBitmpa(String imageString) {
        byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.scanner_menu, menu); //your file name
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.btnInit: {
                InitScanner();

            }
            break;
            case R.id.btnUninit: {
                UnInitScanner();

            }
            break;
            case R.id.btnSyncCapture: {
                scannerAction = ScannerAction.Capture;
                if (!isCaptureRunning) {
                    StartSyncCapture();
                }
            }
            break;
            case R.id.btnStopCapture: {
                StopCapture();
            }
            break;
            case R.id.btnMatchISOTemplate: {
                scannerAction = ScannerAction.Verify;
                if (!isCaptureRunning) {
                    StartSyncCapture();

                }
            }

            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        if (mfs100 == null) {
            mfs100 = new MFS100(this);
            mfs100.SetApplicationContext(StudentDetailsActiviy.this);
        } else {
            InitScanner();
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (mfs100 == null) {
            mfs100 = new MFS100(this);
            mfs100.SetApplicationContext(StudentDetailsActiviy.this);
        } else {
            InitScanner();
        }
        super.onResume();
    }

    private void StopCapture() {
        try {
            mfs100.StopAutoCapture();
        } catch (Exception e) {
            SetTextOnUIThread("Error");
        }
    }

    @Override
    protected void onDestroy() {
        if (mfs100 != null) {
            mfs100.Dispose();
        }
        super.onDestroy();
    }

    private void InitScanner() {
        try {
            int ret = mfs100.Init();
            if (ret != 0) {
                SetTextOnUIThread(mfs100.GetErrorMsg(ret));
            } else {
                SetTextOnUIThread("Init success");
                String info = "Serial: " + mfs100.GetDeviceInfo().SerialNo()
                        + " Make: " + mfs100.GetDeviceInfo().Make()
                        + " Model: " + mfs100.GetDeviceInfo().Model()
                        + "\nCertificate: " + mfs100.GetCertification();

                SetLogOnUIThread(info);

            }
        } catch (Exception ex) {

            SetTextOnUIThread("Init failed, unhandled exception");
        }
    }

    private void StartSyncCapture() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                SetTextOnUIThread("");
                isCaptureRunning = true;
                try {
                    FingerData fingerData = new FingerData();
                    int ret = mfs100.AutoCapture(fingerData, timeout, false);
                    Log.e("StartSyncCapture.RET", "" + ret);
                    if (ret != 0) {
                        SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                    } else {
                        lastCapFingerData = fingerData;
                        final Bitmap bitmap = BitmapFactory.decodeByteArray(fingerData.FingerImage(), 0,
                                fingerData.FingerImage().length);
                        StudentDetailsActiviy.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fmscanPrintImageView.setImageBitmap(bitmap);
                            }
                        });

                        SetTextOnUIThread("Capture Success");
                        String log = "\nQuality: " + fingerData.Quality()
                                + "\nNFIQ: " + fingerData.Nfiq()
                                + "\nWSQ Compress Ratio: "
                                + fingerData.WSQCompressRatio()
                                + "\nImage Dimensions (inch): "
                                + fingerData.InWidth() + "\" X "
                                + fingerData.InHeight() + "\""
                                + "\nImage Area (inch): " + fingerData.InArea()
                                + "\"" + "\nResolution (dpi/ppi): "
                                + fingerData.Resolution() + "\nGray Scale: "
                                + fingerData.GrayScale() + "\nBits Per Pixal: "
                                + fingerData.Bpp() + "\nWSQ Info: "
                                + fingerData.WSQInfo();
                        SetLogOnUIThread(log);
                        //  SetData2(fingerData);
                        // ExtractANSITemplate1(fingerstring);
                        matchCode(ExtractANSITemplate());
                    }
                } catch (Exception ex) {
                    SetTextOnUIThread("Error333"+ex);
                } finally {
                    isCaptureRunning = false;
                }
            }
        }).start();
    }


    private void UnInitScanner() {
        try {
            int ret = mfs100.UnInit();
            if (ret != 0) {
                SetTextOnUIThread(mfs100.GetErrorMsg(ret));
            } else {
                SetLogOnUIThread("Uninit Success");
                SetTextOnUIThread("Uninit Success");
                lastCapFingerData = null;
            }
        } catch (Exception e) {
            Log.e("UnInitScanner.EX", e.toString());
        }
    }

    private void SetTextOnUIThread(final String str) {

        txtEventLog.post(new Runnable() {
            public void run() {
                txtEventLog.setText(str);
                Log.wtf("response error", str);
            }
        });
    }

    private void SetLogOnUIThread(final String str) {

//        scannerAction = ScannerAction.Capture;
//        if (!isCaptureRunning) {
//            StartSyncCapture();
//        }

        txtEventLog.post(new Runnable() {
            public void run() {
                txtEventLog.append("\n" + str);
            }
        });
    }

    public void SetData2(FingerData fingerData) {
        if (scannerAction.equals(ScannerAction.Capture)) {
            Enroll_Template = new byte[fingerData.ISOTemplate().length];
            System.arraycopy(fingerData.ISOTemplate(), 0, Enroll_Template, 0,
                    fingerData.ISOTemplate().length);
        } else if (scannerAction.equals(ScannerAction.Verify)) {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bmp = getBitmpa(fingureImage);
            bmp.compress(Bitmap.CompressFormat.PNG, fingerData.Quality(), stream);
            byte[] byteArray = stream.toByteArray();
            bmp.recycle();

            Verify_Template = new byte[fingerData.ISOTemplate().length];
            System.arraycopy(fingerData.ISOTemplate(), 0, Verify_Template, 0,
                    fingerData.ISOTemplate().length);


            int ret = mfs100.MatchISO(byteArray, Verify_Template);
            if (ret < 0) {
                SetTextOnUIThread("Error: " + ret + "(" + mfs100.GetErrorMsg(ret) + ")");
            } else {
                if (ret >= 1400) {
                    SetTextOnUIThread("Finger matched with score: " + ret);
                } else {
                    SetTextOnUIThread("Finger not matched, score: " + ret);
                }
            }
        }

    }

    private void WriteFile(String filename, byte[] bytes) {
        try {
            String path = Environment.getExternalStorageDirectory()
                    + "//FingerData";
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
            path = path + "//" + filename;
            file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream stream = new FileOutputStream(path);
            stream.write(bytes);
            stream.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void OnDeviceAttached(int vid, int pid, boolean hasPermission) {
        int ret;
        if (!hasPermission) {
            SetTextOnUIThread("Permission denied");
            return;
        }
        if (vid == 1204 || vid == 11279) {
            if (pid == 34323) {
                ret = mfs100.LoadFirmware();
                if (ret != 0) {
                    SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                } else {
                    SetTextOnUIThread("Load firmware success");
                }
            } else if (pid == 4101) {
                String key = "Without Key";
                ret = mfs100.Init();
                if (ret == 0) {
                    showSuccessLog(key);
                } else {
                    SetTextOnUIThread(mfs100.GetErrorMsg(ret));
                }

            }
        }
    }

    private void showSuccessLog(String key) {
        SetTextOnUIThread("Init success");
        String info = "\nKey: " + key + "\nSerial: "
                + mfs100.GetDeviceInfo().SerialNo() + " Make: "
                + mfs100.GetDeviceInfo().Make() + " Model: "
                + mfs100.GetDeviceInfo().Model()
                + "\nCertificate: " + mfs100.GetCertification();
        SetLogOnUIThread(info);
    }

    @Override
    public void OnDeviceDetached() {
        UnInitScanner();
        SetTextOnUIThread("Device removed");
    }

    @Override
    public void OnHostCheckFailed(String err) {
        try {
            SetLogOnUIThread(err);
            Toast.makeText(this, err, Toast.LENGTH_LONG).show();
        } catch (Exception ignored) {
        }
    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }
    /********Convrting old string fom database ********/

    private   byte[] ExtractANSITemplate1(String fingerstring) {

        byte[] tempData = new byte[2000]; // length 2000 is mandatory
        byte[] ansiTemplate;


        byte[] data = Base64.decode(fingerstring, Base64.DEFAULT);
        int dataLen = mfs100.ExtractANSITemplate(data, tempData);
        if (dataLen <= 0) {
            if (dataLen == 0) {
                SetTextOnUIThread("Failed to extract ANSI Template");
            } else {
                SetTextOnUIThread("Failedss"+mfs100.GetErrorMsg(dataLen));
            }
        } else {
            ansiTemplate = new byte[dataLen];
            System.arraycopy(tempData, 0, ansiTemplate, 0, dataLen);
            //  WriteFile("ANSITemplate.ansi", ansiTemplate);
            SetTextOnUIThread("Extract ANSI Template Success");
            return  ansiTemplate;
        }
        return null;
    }
    private   byte[] ExtractANSITemplate() {
        try {
            if (lastCapFingerData == null) {
                SetTextOnUIThread("Finger not capture");
                return null;
            }
            byte[] tempData = new byte[2000]; // length 2000 is mandatory
            byte[] ansiTemplate;
            int dataLen = mfs100.ExtractANSITemplate(lastCapFingerData.RawData(), tempData);
            if (dataLen <= 0) {
                if (dataLen == 0) {
                    SetTextOnUIThread("Failed to extract ANSI Template");
                } else {
                    SetTextOnUIThread(mfs100.GetErrorMsg(dataLen));
                }
            } else {
                ansiTemplate = new byte[dataLen];
                System.arraycopy(tempData, 0, ansiTemplate, 0, dataLen);
                //  WriteFile("ANSITemplate.ansi", ansiTemplate);

                SetTextOnUIThread("Extract ANSI Template Success");
                return  ansiTemplate;
            }
        } catch (Exception e) {
            Log.e("Error", "Extract ANSI Template Error", e);
        }
        return  null;
    }

    public void matchCode( byte[] ANSITemplate1) {
        byte[] data = Base64.decode(fingerstring, Base64.DEFAULT);
        int ret = mfs100.MatchANSI(ANSITemplate1, data);
        if (ret >= 0) {
            String currentDate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
            if (ret >= 4000) {
                SetTextOnUIThread("Fingerprint matched" );
                LogDetailObject logDetailObject = new LogDetailObject(Integer.parseInt(roll_no),currentDate,"Candidate finger print matching for "+finalText,1);
                db.insertLogDetail(logDetailObject);
            } else {
                SetTextOnUIThread("Fingerprint not matched");
                LogDetailObject logDetailObject = new LogDetailObject(Integer.parseInt(roll_no),currentDate,"Candidate finger print matching for "+finalText,0);
                db.insertLogDetail(logDetailObject);
//                fingerPrintCount++;
//                if (fingerPrintCount > 4) {
//                    LogDetailObject logDetailObject = new LogDetailObject(Integer.parseInt(roll_no),currentDate,"Candidate finger print matching for "+finalText,0);
//                    db.insertLogDetail(logDetailObject);
//                }
            }
        } else SetTextOnUIThread(mfs100.GetErrorMsg(ret));
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }


    private void cameraIntent(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
            }else {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 100);
            }

        }else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 100);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100){
            if (resultCode == Activity.RESULT_OK) {
                onCaptureImageResult(data);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap bitmapSelected = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmapSelected.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        byte [] image = bytes.toByteArray();
        boolean isOldImage = db.updateStudentNewImage(image,roll_no);
//        boolean isOldImage = db.insertImage(roll_no,image);
        if (isOldImage) {
            fmStudentImageViewOld.setImageBitmap(bitmapSelected);
        }else {
            fmStudentImageViewNew.setImageBitmap(bitmapSelected);
        }
        String currentDate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        LogDetailObject logDetailObject = new LogDetailObject(Integer.parseInt(roll_no),currentDate,"Capture Candidate Image for "+finalText,1);
        db.insertLogDetail(logDetailObject);

    }

}