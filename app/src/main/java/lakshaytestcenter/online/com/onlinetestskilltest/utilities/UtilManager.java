package lakshaytestcenter.online.com.onlinetestskilltest.utilities;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import lakshaytestcenter.online.com.onlinetestskilltest.BuildConfig;

/**
 * Created by Pradeep on 11,December,2019
 */
public class UtilManager {

    public static final String ATTENDANCE_DIRECTORY = "Attendance";

    public static Bitmap loadImageFromStorage(String path) {
        Bitmap bitmap = null;
        try {

            if (path!=null) {
                File f = new File(path);
                bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        }

        return bitmap;
    }

    public static String saveToInternalStorage(Context context, String directoryName, Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir(directoryName, Context.MODE_PRIVATE);
        if (!directory.exists())
        {
            directory.mkdirs();
        }
        // Create imageDir
        File mypath=new File(directory, fileName);
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return mypath.getAbsolutePath();

    }

    public static Bitmap getCompressedBitmap(Bitmap bitmap){

        int MAX_IMAGE_SIZE = 1024 * 1024; // Max 1 MB
        int streamLength = MAX_IMAGE_SIZE;
        int compressQuality = 105;

        ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
        while (streamLength >= MAX_IMAGE_SIZE && compressQuality > 5) {
            try {
                bmpStream.flush();//to avoid out of memory error
                bmpStream.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
            compressQuality -= 5;
            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            if(BuildConfig.DEBUG) {
                Log.d("test upload", "Quality: " + compressQuality);
                Log.d("test upload", "Size: " + streamLength);
            }
        }

        return bitmap;
    }

    public static Bitmap getBitmpapFromBase64String(String imageString) {
        if (imageString != null) {
            if (!imageString.isEmpty() && imageString.equalsIgnoreCase("null"))
                return null;
            byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return decodedByte;
        }
        return  null;
    }
}
